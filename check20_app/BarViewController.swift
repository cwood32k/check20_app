//
//  BarViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/28/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

import MapKit
import MediaPlayer

class BarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var navButton: UIButton!
    
    var bar: Bar!
    var promos: NSMutableArray = []
    var NO_PROMOS_MESSAGE = "No active promos"
    
    @IBOutlet var infoScrollView: UIScrollView!
    @IBOutlet var mediaScrollView: UIScrollView!
    
    var moviePlayer:MPMoviePlayerController!
    
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var checkInButton: UIButton!
    
    @IBOutlet var name: UILabel!
    @IBOutlet var tagline: UILabel!
    @IBOutlet var phone_number: UIButton!
    @IBOutlet var website: UIButton!
    @IBOutlet var about_us: UITextView!
    
    @IBOutlet var update_my_places: UIButton!
    
    @IBOutlet var currentCheckInsLabel: UILabel!
    @IBOutlet var currentLikesLabel: UILabel!
    @IBOutlet var followersLabel: UILabel!
    
    @IBOutlet var map_view: MKMapView!
    var addressDictionary: [NSObject : AnyObject]?
    
    @IBOutlet var avatar: UIImageView!
    @IBOutlet var video_thumb: UIImageView!
    @IBOutlet var outside_image: UIImageView!
    @IBOutlet var inside_image1: UIImageView!
    @IBOutlet var inside_image2: UIImageView!
    @IBOutlet var inside_image3: UIImageView!
    
    @IBOutlet var promos_tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadImages()
        
        name.text = bar.name
        tagline.text = bar.tagline
        phone_number.setTitle(bar.phone_number, forState: .Normal)
        website.setTitle(bar.website, forState: .Normal)
        about_us.text = bar.about_us
        website.titleLabel!.adjustsFontSizeToFitWidth = true
        
        followersLabel.text = "\(bar.female_followers) girls and \(bar.male_followers) guys following"
        currentCheckInsLabel.text = "\(bar.female_check_ins) girls and \(bar.male_check_ins) guys checked in right now"
        currentLikesLabel.text = "\(bar.female_likes) girls and \(bar.male_likes) guys liked this right now"
        
        let avatar_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.avatar_path! : bar.avatar_path!
        avatar.image = UIImage(data: NSData(contentsOfURL: NSURL(string: avatar_url_string )! )!)
        
        if bar.following {
            update_my_places.setImage(UIImage(named: "remove_from_my_places"), forState: .Normal)
        } else {
            update_my_places.setImage(UIImage(named: "add_to_my_places"), forState: .Normal)
        }
        
        if bar.checked_in {
            checkInButton.setImage(UIImage(named: "checked_in_button"), forState: .Normal)
        } else {
            checkInButton.setImage(UIImage(named: "check_in_button"), forState: .Normal)
        }
        
        if bar.liked {
            likeButton.setImage(UIImage(named: "liked_button"), forState: .Normal)
        } else {
            likeButton.setImage(UIImage(named: "like_button"), forState: .Normal)
        }
        
        loadPromos()
        setupMapView()
        
        setStyles()
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        name.adjustsFontSizeToFitWidth = true
    }
    
    func loadImages() {
        if !bar.video_thumb_path!.hasSuffix("/missing_thumb.png") {
            let video_thumb_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.video_thumb_path! : bar.video_thumb_path!
            let video_thumb_url = NSURL(string:video_thumb_url_string)!
            let videoRequest: NSURLRequest = NSURLRequest(URL: video_thumb_url)
            NSURLConnection.sendAsynchronousRequest(videoRequest, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                dispatch_async(dispatch_get_main_queue(), {
                    self.video_thumb.image = UIImage(data: data)
                })
            })
        } else {
            shiftViews()
        }
        
        let image1_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.inside_image1_path! : bar.inside_image1_path!
        let image1_url = NSURL(string:image1_url_string)!
        let request1: NSURLRequest = NSURLRequest(URL: image1_url)
        NSURLConnection.sendAsynchronousRequest(request1, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.inside_image1.image = UIImage(data: data)
            })
        })
        
        let image2_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.inside_image2_path! : bar.inside_image2_path!
        let image2_url = NSURL(string:image2_url_string)!
        let request2: NSURLRequest = NSURLRequest(URL: image2_url)
        NSURLConnection.sendAsynchronousRequest(request2, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.inside_image2.image = UIImage(data: data)
            })
        })
        
        let image3_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.inside_image3_path! : bar.inside_image3_path!
        let image3_url = NSURL(string:image3_url_string)!
        let request3: NSURLRequest = NSURLRequest(URL: image3_url)
        NSURLConnection.sendAsynchronousRequest(request3, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.inside_image3.image = UIImage(data: data)
            })
        })
        
        let image4_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.outside_image_path! : bar.outside_image_path!
        let image4_url = NSURL(string:image4_url_string)!
        let request4: NSURLRequest = NSURLRequest(URL: image4_url)
        NSURLConnection.sendAsynchronousRequest(request4, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.outside_image.image = UIImage(data: data)
            })
        })
        
        mediaScrollView.contentSize = CGSizeMake(self.view.frame.width, outside_image.frame.height + outside_image.frame.origin.y + 10)
    }
    
    func shiftViews() {
        video_thumb.removeFromSuperview()
        inside_image1.setTranslatesAutoresizingMaskIntoConstraints(false)
        inside_image2.setTranslatesAutoresizingMaskIntoConstraints(false)
        inside_image3.setTranslatesAutoresizingMaskIntoConstraints(false)
        outside_image.setTranslatesAutoresizingMaskIntoConstraints(false)
        var topSpace1 = NSLayoutConstraint(item: inside_image1, attribute: .Top, relatedBy: .Equal, toItem: mediaScrollView, attribute: .Top, multiplier: 1, constant: inside_image1.frame.origin.y - inside_image1.frame.height )
        var topSpace2 = NSLayoutConstraint(item: inside_image2, attribute: .Top, relatedBy: .Equal, toItem: mediaScrollView, attribute: .Top, multiplier: 1, constant: inside_image2.frame.origin.y - inside_image2.frame.height )
        var topSpace3 = NSLayoutConstraint(item: inside_image3, attribute: .Top, relatedBy: .Equal, toItem: mediaScrollView, attribute: .Top, multiplier: 1, constant: inside_image3.frame.origin.y - inside_image3.frame.height )
        var topSpace4 = NSLayoutConstraint(item: outside_image, attribute: .Top, relatedBy: .Equal, toItem: mediaScrollView, attribute: .Top, multiplier: 1, constant: outside_image.frame.origin.y - outside_image.frame.height )
        mediaScrollView.addConstraint(topSpace1)
        mediaScrollView.addConstraint(topSpace2)
        mediaScrollView.addConstraint(topSpace3)
        mediaScrollView.addConstraint(topSpace4)
        
        inside_image1.frame.origin.y -= inside_image1.frame.height
        inside_image2.frame.origin.y -= inside_image2.frame.height
        inside_image3.frame.origin.y -= inside_image3.frame.height
        outside_image.frame.origin.y -= outside_image.frame.height
    }
    
    func loadPromos() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath("/bars/\(bar.id)/promos", parameters: nil,
            success: { operation, mappingResult in
                for promo in mappingResult.array() as [Promo]
                {
                    self.promos.addObject(promo)
                }
                
                if self.promos.count == 0 {
                    var promo = Promo()
                    promo.name = self.NO_PROMOS_MESSAGE
                    self.promos.addObject(promo)
                }
                self.promos_tableView.reloadData()
                
            }, failure: { operation, error in
                 println("an error occured: \(error)")
            })
    }
    
    func setupMapView() {
        var location = "\(bar.address1) \(bar.address2)";
        var geocoder:CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler:{ placemarks, error in
            if (placemarks != nil && placemarks.count > 0) {
                var topResult: CLPlacemark = placemarks[0] as CLPlacemark
                var placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                
                self.addressDictionary = placemark.addressDictionary
                
                var region: MKCoordinateRegion = self.map_view.region;
                region.center = placemark.location.coordinate
                region.span.longitudeDelta /= 1024.0;
                region.span.latitudeDelta /= 1024.0;
                
                var annotation: MKPointAnnotation = MKPointAnnotation()
                annotation.coordinate = placemark.coordinate
                annotation.title = self.bar.name
                annotation.subtitle = "Click to get directions..."
                
                self.map_view.setRegion(region, animated:true)
                self.map_view.addAnnotation(annotation)
                }
            })
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "location_pin"
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as MKAnnotationView?
        
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
        }
        else {
            pinView!.annotation = annotation
        }
        
        pinView!.image = UIImage(named: "map_pin")
        pinView!.centerOffset = CGPointMake(0, -pinView!.image.size.height / 2);
        pinView!.rightCalloutAccessoryView = UIButton.buttonWithType(.InfoDark) as UIButton
        
        return pinView
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!,
        calloutAccessoryControlTapped control: UIControl!) {
            var coords = view.annotation.coordinate
            Navigation.openMapWithCoordinates(coords.latitude, lng: coords.longitude, location: bar, addressDictionary: addressDictionary!)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("PromoTableViewCell") as PromoTableViewCell
        cell.titleLabel.text = promos[indexPath.row].name
        if promos[indexPath.row].name == NO_PROMOS_MESSAGE {
            cell.userInteractionEnabled = false
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as PromoTableViewCell
        showHighlight(cell)
        self.navigationController?.pushViewController(Navigation.promo(promos[indexPath.row].id, brand_new: false), animated: true)
    }
    
    func showHighlight(cell: PromoTableViewCell) {
        UIView.transitionWithView(cell, duration: 0.001, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            cell.titleLabel.textColor = Constants.CHECK20_LIGHT_ORANGE
            }, completion: { _ in
                UIView.transitionWithView(cell, duration: 0.55, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    cell.titleLabel.textColor = UIColor.whiteColor()
                    }, completion: nil)
        })
    }
    
    func updateLikeUI() {
        likeButton.setImage(UIImage(named: "liked_button"), forState: .Normal)
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var user = NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User
        var user_gender = user.gender
        if user_gender == "M" {
            self.bar.male_likes = NSNumber(double: self.bar.male_likes.doubleValue + 1.0)
        } else {
            self.bar.female_likes = NSNumber(double: self.bar.female_likes.doubleValue + 1.0)
        }
        self.currentLikesLabel.text = "\(self.bar.female_likes) girls and \(self.bar.male_likes) guys liked this right now"
    }
    
    func updateCheckedInUI() {
        checkInButton.setImage(UIImage(named: "checked_in_button"), forState: .Normal)
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var user = NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User
        var user_gender = user.gender
        if user_gender == "M" {
            self.bar.male_check_ins = NSNumber(double: self.bar.male_check_ins.doubleValue + 1.0)
        } else {
            self.bar.female_check_ins = NSNumber(double: self.bar.female_check_ins.doubleValue + 1.0)
        }
        self.currentCheckInsLabel.text = "\(self.bar.female_check_ins) girls and \(self.bar.male_check_ins) guys checked in right now"
    }
    
    func updateFollowingUI(added: Bool) {
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var user = NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User
        var user_gender = user.gender
        var inc: Double = added ? 1 : -1
        if user_gender == "M" {
            self.bar.male_followers = NSNumber(double: self.bar.male_followers.doubleValue + inc)
        } else {
            self.bar.female_followers = NSNumber(double: self.bar.female_followers.doubleValue + inc)
        }
        self.followersLabel.text = "\(self.bar.female_followers) girls and \(self.bar.male_followers) guys following"
    }
    
    @IBAction func playVideo(recognizer: UITapGestureRecognizer) {
        UIApplication.sharedApplication().setStatusBarOrientation(.LandscapeLeft, animated: true)
        
        var url:NSURL = Constants.ENV == "development" ? NSURL(string: Constants.API_HOST + bar.video_path!)! : NSURL(string: bar.video_path!)!
        
        var playerVC = VideoPlayerController(contentURL: url)
        
        NSNotificationCenter.defaultCenter().removeObserver(playerVC,
            name:MPMoviePlayerPlaybackDidFinishNotification,
            object:playerVC.moviePlayer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"movieFinishedCallback:", name:MPMoviePlayerPlaybackDidFinishNotification, object:playerVC.moviePlayer)
        
        self.presentMoviePlayerViewControllerAnimated(playerVC)
        
        
        playerVC.moviePlayer.prepareToPlay()
        playerVC.moviePlayer.play()
    }
    
    func movieFinishedCallback(notification: NSNotification) {
        let userInfo:Dictionary<NSObject, AnyObject?> = notification.userInfo!
        var reason = userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] as Int
        if reason != MPMovieFinishReason.PlaybackEnded.rawValue {
            self.dismissMoviePlayerViewControllerAnimated()
        }
    }
    
    @IBAction func like(sender: UIButton) {
        RKObjectManager.sharedManager().HTTPClient.postPath(Constants.API_BAR_LIKE_PATH + "\(bar.id)", parameters: nil, success: { operation, _ in
            if operation.response.statusCode == 201 {
                self.updateLikeUI()
            }
        }, failure: { error in
                
        })
    }
    
    @IBAction func checkIn(sender: UIButton) {
        RKObjectManager.sharedManager().HTTPClient.postPath(Constants.API_BAR_CHECK_IN_PATH + "\(bar.id)", parameters: nil, success: { operation, _ in
            if operation.response.statusCode == 201 {
                self.updateCheckedInUI()
            }
        }, failure: { error in
                
        })
    }
    
    @IBAction func updateMyPlaces(sender: UIButton) {
        
        if bar.following {
            RKObjectManager.sharedManager().HTTPClient.deletePath(Constants.API_UPDATE_MY_PLACES_PATH, parameters: ["bar_id": bar.id], success: { operation, _ in
                if operation.response.statusCode == 200 {
                    sender.setImage(UIImage(named: "add_to_my_places"), forState: .Normal)
                    self.bar.following = false
                    self.updateFollowingUI(false)
                }
                }, failure: { error in
                    
            })
        } else {
            RKObjectManager.sharedManager().HTTPClient.postPath(Constants.API_UPDATE_MY_PLACES_PATH, parameters: ["bar_id": bar.id], success: { operation, _ in
                    if operation.response.statusCode == 200 {
                        sender.setImage(UIImage(named: "remove_from_my_places"), forState: .Normal)
                        self.bar.following = true
                        self.updateFollowingUI(true)
                    }
                }, failure: { error in
                
            })
        }
    }
    
    @IBAction func callVenue(sender: UIButton) {
        var alertController = UIAlertController(title: "Call \(bar.name)?", message: bar.phone_number, preferredStyle: .Alert)
        
        var callAction = UIAlertAction(title: "Call", style: UIAlertActionStyle.Default) { UIAlertAction in
            var strippedNumber = self.phone_number.titleLabel!.text!.stringByReplacingOccurrencesOfString("-", withString: "", options: nil, range: nil)
            var url = NSURL(string: "tel://\(strippedNumber)")!
            UIApplication.sharedApplication().openURL(url)
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(callAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func openWebsite(sender: UIButton) {
        var website_string = website.titleLabel!.text!
        var url = NSURL(string: "http://\(website_string)")!
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func toggleViews(sender: UISegmentedControl) {
        if mediaScrollView.hidden {
            mediaScrollView.hidden = false
            infoScrollView.hidden = true
        } else {
            mediaScrollView.hidden = true
            infoScrollView.hidden = false
        }
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
