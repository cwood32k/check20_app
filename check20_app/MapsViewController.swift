//
//  MapsViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/14/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

import UIKit
import MapKit

class MapsViewController : UIViewController, MKMapViewDelegate {
    
    @IBOutlet var navButton: UIButton!
    @IBOutlet var headerRightButton: UIButton!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchField: UITextField!
    
    @IBOutlet var map_view: MKMapView!
    
    var bars = [Bar]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadActivityIndicator()
        actInd.startAnimating()
        
        loadBars()
        
        setStyles()
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if !searchView.hidden {
            searchField.resignFirstResponder()
            toggleSearchView(false)
        }
    }
    
    func toggleSearchView(show: Bool) {
        UIView.transitionWithView(searchView, duration: 0.35, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.searchView.hidden = !show
            if self.searchView.hidden {
                self.searchField.resignFirstResponder()
                self.searchField.text = ""
            }
            }, completion:nil )
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        headerRightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func loadBars() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath(Constants.API_BARS_PATH, parameters: nil,
            success: { operation, mappingResult in
                
                self.bars = mappingResult.array() as [Bar]
                
                self.setupMapView()
                
            }, failure: { operation, error in
                println("an error occured: \(error)")
            }
        )
    }
    
    func setupMapView() {
        var completed = 0
        for bar:Bar in bars {
            completed++
            var location = "\(bar.address1) \(bar.address2)";
            var geocoder:CLGeocoder = CLGeocoder()
            geocoder.geocodeAddressString(location, completionHandler:{ placemarks, error in
                if (placemarks != nil && placemarks.count > 0) {
                    var topResult: CLPlacemark = placemarks[0] as CLPlacemark
                    var placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                    
                    var annotation: MKPointAnnotation = MKPointAnnotation()
                    annotation.coordinate = placemark.coordinate
                    annotation.title = bar.name
                    annotation.subtitle = bar.tagline
                    
                    self.map_view.addAnnotation(annotation)
                }
                if completed == self.bars.count {
                    self.actInd.stopAnimating()
                    self.map_view.region = self.regionToFitLocations(self.map_view.annotations as [MKAnnotation], coordCount: self.map_view.annotations.count)
                }
            })
        }
    }
    
    func regionToFitLocations(annotations : [MKAnnotation], coordCount : Int) -> MKCoordinateRegion {
        var r: MKMapRect = MKMapRectNull
        for (var i=0; i < coordCount; ++i) {
            var p: MKMapPoint = MKMapPointForCoordinate(annotations[i].coordinate)
            r = MKMapRectUnion(r, MKMapRectMake(p.x, p.y, 0, 0))
        }
        return MKCoordinateRegionForMapRect(r)
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        
        let reuseId = "location_pin"
        
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as MKAnnotationView?
        
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
        }
        else {
            pinView!.annotation = annotation
        }
        
        pinView!.image = UIImage(named: "map_pin")
        pinView!.centerOffset = CGPointMake(0, -pinView!.image.size.height / 2);
        
        var bar: Bar = bars.filter() { $0.name == annotation.title && $0.tagline == annotation.subtitle }[0]
        let avatar_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.avatar_path! : bar.avatar_path!
        let url: NSURL = NSURL(string:avatar_url_string)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        
        pinView!.rightCalloutAccessoryView = UIButton.buttonWithType(.InfoDark) as UIButton
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if error == nil {
                var imageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
                imageButton.setImage(UIImage(data: data), forState: .Normal)
                pinView!.leftCalloutAccessoryView = imageButton
            }
        })
        
        return pinView
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!,
        calloutAccessoryControlTapped control: UIControl!) {
            var bar: Bar = bars.filter() { $0.name == view.annotation.title && $0.tagline == view.annotation.subtitle }[0]
            navigationController?.pushViewController(Navigation.bar(&bar), animated: true)
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        toggleSearchView(searchView.hidden)
        searchField.becomeFirstResponder()
    }
    
    @IBAction func search(sender: UIButton) {
        let searchText = searchField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if countElements(searchText) > 0 {
            self.navigationController?.pushViewController(Navigation.searchResults(searchText), animated:true)
        } else {
            
        }
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func cancelSearch(sender: UIButton) {
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
}
