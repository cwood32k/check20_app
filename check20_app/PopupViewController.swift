//
//  PopupViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/15/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class PopupViewController: UIViewController {
    @IBOutlet var header: UILabel!
    @IBOutlet var message: UITextView!
    @IBOutlet var ok_button: UIButton!
    
    var tag: Int!
    /*
        -1 => redirect to homescreen
    */
    
    var header_text: String?
    var message_text: String?
    
    override func viewDidLoad() {
        header.font = UIFont(name: "Roboto-Bold", size: 18)
        message.font = UIFont(name: "Roboto-Regular", size: 12)
        header.text = header_text
        message.text = message_text
    }
    
    override func viewDidAppear(animated: Bool) {
        view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
    }
    
    
    @IBAction func okPressed() {
        switch tag
        {
        case -1:
            var presenter: UINavigationController = self.presentingViewController as UINavigationController
            presenter.dismissViewControllerAnimated(false, completion: {
                presenter.popToRootViewControllerAnimated(true)
                return
            })
        default:
            self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        }
    }

}
