//
//  Answer.swift
//  check20_app
//
//  Created by Chris Wood on 2/15/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class Answer : NSObject {
    var question_num: NSNumber!
    var question_id: NSNumber!
    var user_id: NSNumber!
    var choice: NSNumber!
    
    override init()
    {
        super.init()
    }
}