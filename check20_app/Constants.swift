//
//  Constants.swift
//  check20_app
//
//  Created by Chris Wood on 10/11/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

struct Constants
{
    static let CHECK20_DARK_ORANGE = UIColor(red: 182/255.0, green: 68/255.0, blue: 19/255.0, alpha: 1.0)
    static let CHECK20_LIGHT_ORANGE = UIColor(red: 230/255.0, green: 146/255.0, blue: 0/255.0, alpha: 1.0)
    static let CHECK20_LIGHT_GREY = UIColor(red: 65/255.0, green: 62/255.0, blue: 71/255.0, alpha: 1.0)
    static let CHECK20_DARK_GREY = UIColor(red: 47/255.0, green: 45/255.0, blue: 51/255.0, alpha: 1.0)
    
    static let ENV = "development"
    
    static let USER_KEY = "check20.user"
    static let DEVICE_TOKEN_KEY = "check20.device_token"
    //static let API_HOST = "http://still-mountain-7891.herokuapp.com"
    static let API_HOST = "http://192.168.1.2:3000"
    //static let API_HOST = "http://api.check20.dev"
    static let API_QUESTION_PATH = "/question/:id"
    static let API_NEXT_QUESTION_PATH = "/next_question"
    static let API_SAVE_ANSWER_PATH = "/save_answer"
    static let API_QUESTIONS_PATH = "/questions"
    static let API_MY_PLACES_PATH = "/my_places"
    static let API_BARS_PATH = "/bars"
    static let API_BAR_PATH = "/bars/:id"
    static let API_BAR_LIKE_PATH = "/bars/like/"
    static let API_BAR_CHECK_IN_PATH = "/bars/check_in/"
    static let API_BEACON_CHECK_IN_PATH = "/beacons/check_in/"
    static let API_PROMOS_PATH = "/promos"
    static let API_PROMO_PATH = "/promos/:id"
    static let API_BAR_PROMOS_PATH = "/bars/:id/promos"
    static let API_USERS_PATH = "/users"
    static let API_USER_PATH = "/users/:id"
    static let API_LOGIN_PATH = "/sessions"
    static let API_TWITTER_LOGIN_PATH = "/twitter_login"
    static let API_REGISTER_PATH = "/users"
    static let API_REGISTER_DEVICE_PATH = "/register_device"
    static let IS_DEVICE_REGISTERED_KEY = "check20.device_registered"
    static let API_UPDATE_MY_PLACES_PATH = "/update_my_places"
    static let API_SEARCH_PATH = "/search"
}