//
//  AppDelegate.swift
//  check20_app
//
//  Created by Chris Wood on 10/8/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SonicDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        var type = UIUserNotificationType.Badge | UIUserNotificationType.Alert | UIUserNotificationType.Sound;
        var setting = UIUserNotificationSettings(forTypes: type, categories: nil);
        UIApplication.sharedApplication().registerUserNotificationSettings(setting);
        UIApplication.sharedApplication().registerForRemoteNotifications();
        
//        Sonic.sharedInstance().initializeWithApplicationGUID("MWIxYzA1N2MtNWU4YS00NWY5LTkyOWMtMDA1NDZjMzE3N2Fj",
//            andDelegate:self, quietOption:false)
//        Sonic.sharedInstance().start()
        
        RestKitManager.getOrCreateSharedInstance
        RestKitManager.setHeaders()
        return true
    }
    
//    func sonic(sonic: Sonic, didHearCode code: SonicCodeHeard) -> Bool {
//        if code.isKindOfClass(SonicBluetoothCodeHeard){
//            let codeHeard = code as SonicBluetoothCodeHeard
//            println("Did hear code \(codeHeard.beaconCode)")
//            var defaults =  NSUserDefaults.standardUserDefaults()
//            var logged_in: Bool = defaults.objectForKey(Constants.USER_KEY) != nil
//            if logged_in {
//                RKObjectManager.sharedManager().HTTPClient.postPath(Constants.API_BEACON_CHECK_IN_PATH + "\(codeHeard.beaconCode)", parameters: nil, success: { operation, _ in
//                    if operation.response.statusCode == 201 {
//                        println("user checked in")
//                    }
//                    }, failure: { error in
//                        
//                })
//            }
//            
//        } else if code.isKindOfClass(SonicAudioHeardCode) {
//            
//            let audioHeard = code as SonicAudioHeardCode
//            println("Did hear code \(audioHeard.beaconCode)")
//            var defaults =  NSUserDefaults.standardUserDefaults()
//            var logged_in: Bool = defaults.objectForKey(Constants.USER_KEY) != nil
//            if logged_in {
//                RKObjectManager.sharedManager().HTTPClient.postPath(Constants.API_BEACON_CHECK_IN_PATH + "\(audioHeard.beaconCode)", parameters: nil, success: { operation, _ in
//                    if operation.response.statusCode == 201 {
//                        println("user checked in")
//                    }
//                    }, failure: { error in
//                        
//                })
//            }
//        } else {
//            println("Did hear code \(code.beaconCode)")
//        }
//        
//        return true
//    }
//    
//    func sonic(sonic: Sonic, didGeoFenceEntered location: SonicLocation) {
//        var id = location.identifier
//        var name = location.name
//        Sonic.sharedInstance().initializeWithApplicationGUID("MWIxYzA1N2MtNWU4YS00NWY5LTkyOWMtMDA1NDZjMzE3N2Fj",
//            andDelegate:self, quietOption:false)
//        Sonic.sharedInstance().start()
//    }
//    
//    func sonic(sonic: Sonic, didGeoFenceExited location: SonicLocation) {
//        Sonic.sharedInstance().stop()
//    }
    
    func application( application: UIApplication!, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData! ) {
        println("My device token is: \(deviceToken)")
        
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(deviceToken.description, forKey: Constants.DEVICE_TOKEN_KEY)
        
    }
    
    func application( application: UIApplication!, didFailToRegisterForRemoteNotificationsWithError error: NSError! ) {
        println("Failed to get token, error: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo:NSDictionary!) {
        if application.applicationState == UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background {
            NotificationsManager.handleOpenedNotification(userInfo)
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        NotificationsManager.updateBadgeNumber()
        NotificationsManager.registerDevice()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "check20.check20_app" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("check20_app", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("check20_app.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain:"YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }

}



