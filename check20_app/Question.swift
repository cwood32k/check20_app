//
//  Question.swift
//  check20_app
//
//  Created by Chris Wood on 2/15/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class Question : NSObject {
    var number: NSNumber!
    var question: String!
    var answer1: String!
    var answer2: String!
    var answer3: String!
    var answer4: String!
    
    var user_choice: NSNumber?
    
    override init()
    {
        super.init()
    }
}
