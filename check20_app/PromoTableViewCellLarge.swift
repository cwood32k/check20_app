//
//  PromoTableViewCellLarge.swift
//  check20_app
//
//  Created by Chris Wood on 2/14/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class PromoTableViewCellLarge: UITableViewCell {
    
    @IBOutlet var bar_name: UILabel!
    @IBOutlet var promo_name: UILabel!
    @IBOutlet var promo_imageView: UIImageView!
    @IBOutlet var promo_start_date: UILabel!
    @IBOutlet var promo_end_date: UILabel!
    
    var promoBackgroundView: UIImageView?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Constants.CHECK20_LIGHT_GREY
        selectionStyle = .None
    }
    
    override var layoutMargins: UIEdgeInsets {
        get { return UIEdgeInsetsZero }
        set(newVal) { }
    }
    
    func setStyles() {
        bar_name.font = UIFont(name:"Roboto-Bold", size:16)
        promo_name.font = UIFont(name:"Roboto-BlackItalic", size:14)
    }
    
    func setBackgroundImage() {
        if promoBackgroundView == nil {
            promoBackgroundView = UIImageView(frame: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(4, 2, 0, 2)))
            
            promoBackgroundView!.image = UIImage(named: "cell_background_grey")
            addSubview(promoBackgroundView!)
            sendSubviewToBack(promoBackgroundView!)
        }
    }
    
    
}