//
//  Notifications.swift
//  check20_app
//
//  Created by Chris Wood on 2/17/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class NotificationsManager: NSObject {
    
    class func registerDevice() {
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var logged_in: Bool = defaults.objectForKey(Constants.USER_KEY) != nil
        var registered_device = defaults.objectForKey(Constants.IS_DEVICE_REGISTERED_KEY) as? Bool
        if (logged_in && NotificationsManager.userHasPushNotificationsEnabled()) {
            if registered_device == nil || !registered_device! {
                var device_token: String = defaults.objectForKey(Constants.DEVICE_TOKEN_KEY) as String
                var path = "\(Constants.API_HOST)\(Constants.API_REGISTER_DEVICE_PATH)"
                RKObjectManager.sharedManager().HTTPClient.postPath(path, parameters: ["device_token":device_token], success: { operation, _ in
                    if operation.response.statusCode == 200 {
                        defaults.setObject(true, forKey: Constants.IS_DEVICE_REGISTERED_KEY)
                    }
                    }, failure: { error in
                        
                })
            }
        } else {
            if registered_device != nil && registered_device! {
                var device_token: String = defaults.objectForKey(Constants.DEVICE_TOKEN_KEY) as String
                var path = "\(Constants.API_HOST)\(Constants.API_REGISTER_DEVICE_PATH)"
                RKObjectManager.sharedManager().HTTPClient.deletePath(path, parameters: ["device_token":device_token], success: { operation, response in
                    if operation.response.statusCode == 200 {
                        defaults.setObject(false, forKey: Constants.IS_DEVICE_REGISTERED_KEY)
                    }
                    }, failure: { error in
                        
                })
            }
        }
    }
    
    class func userHasPushNotificationsEnabled() -> Bool {
        if UIApplication.sharedApplication().respondsToSelector("currentUserNotificationSettings") {
            let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
            return (settings.types & UIUserNotificationType.Alert) != UIUserNotificationType.None
        }
        
        let types = UIApplication.sharedApplication().enabledRemoteNotificationTypes()
        return (types & UIRemoteNotificationType.Alert) != UIRemoteNotificationType.None
    }
    
    class func handleOpenedNotification(userInfo: NSDictionary) {
        var apn = userInfo.objectForKey("aps") as Dictionary<String, AnyObject>
        if let category = apn["category"] as? String {
            switch category
            {
            case "New Promo":
                apn["promo_id"] = userInfo.objectForKey("promo_id")
                NSNotificationCenter.defaultCenter().postNotificationName("New Promo", object:nil, userInfo:apn)
                break
            default:
                break
            }
        }
    }
    
    class func updateBadgeNumber() {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }
}