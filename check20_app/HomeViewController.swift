//
//  HomeViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/29/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class HomeViewController: UIViewController {
    
    @IBOutlet var homeLabel: UILabel!
    @IBOutlet var navButton: UIButton!
    @IBOutlet var headerRightButton: UIButton!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"handleNewPromoNotificaton:", name: "New Promo", object: nil)
        setStyles()
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        headerRightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        homeLabel.font = UIFont(name:"Roboto-Bold", size:18)
        homeLabel.sizeToFit()
    }
    
    override func viewDidAppear(animated: Bool) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if !searchView.hidden {
            searchField.resignFirstResponder()
            toggleSearchView(false)
        }
    }
    
    func toggleSearchView(show: Bool) {
        UIView.transitionWithView(searchView, duration: 0.35, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.searchView.hidden = !show
            if self.searchView.hidden {
                self.searchField.resignFirstResponder()
                self.searchField.text = ""
            }
        }, completion:nil )
    }
    
    func handleNewPromoNotificaton(notification: NSNotification) {
        var apn = notification.userInfo as Dictionary<String, AnyObject>
        var new_promo_id = apn["promo_id"]! as NSNumber
        self.navigationController?.pushViewController(Navigation.promo(new_promo_id, brand_new: true), animated: true)
    }

    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        toggleSearchView(searchView.hidden)
        searchField.becomeFirstResponder()
    }
    
    @IBAction func search(sender: UIButton) {
        let searchText = searchField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if countElements(searchText) > 0 {
            self.navigationController?.pushViewController(Navigation.searchResults(searchText), animated:true)
        } else {
            
        }
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func cancelSearch(sender: UIButton) {
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func mapsPressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.maps(), animated: true)
    }
    
    @IBAction func myPlacesPressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.myPlaces(), animated: true)
    }
    
    @IBAction func trendingPressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.topTrending(), animated: true)
    }
    
    @IBAction func myProfilePressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.myProfile(), animated: true)
    }
    
    @IBAction func promosPressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.promos(), animated: true)
    }
    
    @IBAction func questionsPressed(sender: UIButton) {
        self.navigationController?.pushViewController(Navigation.questions(), animated: true)
    }
}
