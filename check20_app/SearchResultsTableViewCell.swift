//
//  SearchResultsTableViewCell.swift
//  check20_app
//
//  Created by Chris Wood on 2/26/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class SearchResultsTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var resultImageView: UIImageView!
    
    var resultBackgroundView: UIImageView?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Constants.CHECK20_LIGHT_GREY
        selectionStyle = .None
    }
    
    func setStyles() {
        titleLabel.font = UIFont(name:"Roboto-Bold", size:16)
        titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func setBackgroundImage() {
        if resultBackgroundView == nil {
            resultBackgroundView = UIImageView(frame: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(4, 2, 0, 2)))
            
            resultBackgroundView!.image = UIImage(named: "cell_background_grey")
            addSubview(resultBackgroundView!)
            sendSubviewToBack(resultBackgroundView!)
        }
    }
}
