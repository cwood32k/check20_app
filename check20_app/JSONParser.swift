//
//  JSONParser.swift
//  check20_app
//
//  Created by Chris Wood on 3/1/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class JSONParser : NSObject {
    class func barFromDictionaryString(jsonString: String) -> Bar {
        if let data = jsonString.dataUsingEncoding(NSUTF8StringEncoding) {
            if let values = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(0), error: nil)  as? [String: AnyObject] {
                return Bar(values: values)
            }
        }
        
        return Bar()
    }
    
    class func promoFromDictionaryString(jsonString: String) -> Promo {
        if let data = jsonString.dataUsingEncoding(NSUTF8StringEncoding) {
            if let values = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(0), error: nil)  as? [String: AnyObject] {
                return Promo(values: values)
            }
        }
        
        return Promo()
    }

}
