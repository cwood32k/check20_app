//
//  Navigation.swift
//  check20_app
//
//  Created by Chris Wood on 2/16/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

import MapKit

class Navigation : NSObject {
    
    private class func createSideSwipeMenu(centerVC: UIViewController) -> MMDrawerController {
        var sideSwipeScreens: UIStoryboard = UIStoryboard(name:"SideSwipe", bundle:nil)
        var sideSwipeVC = sideSwipeScreens.instantiateViewControllerWithIdentifier("SideSwipeViewController") as SideSwipeViewController
        
        var swipeController: MMDrawerController = MMDrawerController(centerViewController: centerVC, leftDrawerViewController: sideSwipeVC)
        
        swipeController.maximumLeftDrawerWidth = 260
        swipeController.openDrawerGestureModeMask = .PanningCenterView
        swipeController.closeDrawerGestureModeMask = .PanningDrawerView & .PanningCenterView
        swipeController.closeDrawerGestureModeMask = .BezelPanningCenterView
        swipeController.centerHiddenInteractionMode = .Full
        swipeController.showsShadow = false
        
        return swipeController
    }
    
    class func home() -> UIViewController {
        var homeScreens: UIStoryboard = UIStoryboard(name:"Home", bundle:nil)
        var homeVC: HomeViewController = homeScreens.instantiateViewControllerWithIdentifier("HomeViewController") as HomeViewController
        
        var swipeController = createSideSwipeMenu(homeVC)
        
        return swipeController
    }
    
    class func myPlaces() -> UIViewController {
        var barScreens: UIStoryboard = UIStoryboard(name:"Bars", bundle:nil)
        var barsVC = barScreens.instantiateViewControllerWithIdentifier("BarsViewController") as BarsViewController
        
        var swipeController = createSideSwipeMenu(barsVC)
        return swipeController
    }
    
    class func bar(bar: Bar) -> BarViewController {
        var barScreens: UIStoryboard = UIStoryboard(name:"Bars", bundle:nil)
        var view: String = ""
        switch UIDevice().deviceType
        {
        case .iPhone5, .iPhone5c, .iPhone5S:
            view = "BarViewController5"
        case .iPhone6:
            view = "BarViewController6"
        case .iPhone6plus:
            view = "BarViewControllerPlus"
        default:
            view = "BarViewController5"
            break
        }
        
        let barVC = barScreens.instantiateViewControllerWithIdentifier(view)! as BarViewController
        barVC.bar = bar
        return barVC
    }
    
    class func bar(inout bar: Bar) -> BarViewController {
        var barScreens: UIStoryboard = UIStoryboard(name:"Bars", bundle:nil)
        var view: String = ""
        switch UIDevice().deviceType
        {
        case .iPhone5, .iPhone5c, .iPhone5S:
            view = "BarViewController5"
        case .iPhone6:
            view = "BarViewController6"
        case .iPhone6plus:
            view = "BarViewControllerPlus"
        default:
            view = "BarViewController5"
            break
        }
    
        let barVC = barScreens.instantiateViewControllerWithIdentifier(view)! as BarViewController
        barVC.bar = bar
        return barVC
    }
    
    class func maps() -> UIViewController {
        var mapScreens: UIStoryboard = UIStoryboard(name:"Map", bundle:nil)
        var mapsVC = mapScreens.instantiateViewControllerWithIdentifier("MapsViewController") as MapsViewController
        
        var swipeController = createSideSwipeMenu(mapsVC)
        
        return swipeController
    }
    
    class func topTrending() -> UIViewController {
        var trendingScreens: UIStoryboard = UIStoryboard(name:"TopTrending", bundle:nil)
        var trendingVC = trendingScreens.instantiateViewControllerWithIdentifier("TopTrendingViewController") as TopTrendingViewController
        
        var swipeController = createSideSwipeMenu(trendingVC)
        
        return swipeController
    }
    
    class func myProfile() -> UIViewController {
        var profileScreens: UIStoryboard = UIStoryboard(name:"MyProfile", bundle:nil)
        var profileVC = profileScreens.instantiateViewControllerWithIdentifier("MyProfileViewController") as MyProfileViewController
        
        var swipeController = createSideSwipeMenu(profileVC)
        
        return swipeController
    }
    
    class func promos() -> UIViewController {
        var promoScreens: UIStoryboard = UIStoryboard(name:"Promos", bundle:nil)
        var promosVC = promoScreens.instantiateViewControllerWithIdentifier("PromosViewController") as PromosViewController
        
        var swipeController = createSideSwipeMenu(promosVC)
        
        return swipeController
    }
    
    class func promo(promo_id: NSNumber?, brand_new: Bool) -> UIViewController {
        var promoScreens: UIStoryboard = UIStoryboard(name:"Promos", bundle:nil)
        let promoVC = promoScreens.instantiateViewControllerWithIdentifier("PromoDetailsViewController")! as PromoDetailsViewController
        promoVC.promo_id = promo_id
        promoVC.brand_new = brand_new
        return promoVC
    }
    
    class func promo(promo: Promo, image: UIImage?) -> PromoDetailsViewController {
        var promoScreens: UIStoryboard = UIStoryboard(name:"Promos", bundle:nil)
        let promoVC = promoScreens.instantiateViewControllerWithIdentifier("PromoDetailsViewController")! as PromoDetailsViewController
        promoVC.promo = promo
        promoVC.promo_image = image
        return promoVC
    }
    
    class func questions() -> UIViewController {
        var questionScreens: UIStoryboard = UIStoryboard(name:"Questions", bundle:nil)
        var questionsVC = questionScreens.instantiateViewControllerWithIdentifier("QuestionsViewController") as QuestionsViewController
        
        var swipeController = createSideSwipeMenu(questionsVC)
        
        return swipeController
    }
    
    class func question(inout questions: [Question], index: Int) -> UIViewController {
        var questionScreens: UIStoryboard = UIStoryboard(name:"Questions", bundle:nil)
        var questionVC = questionScreens.instantiateViewControllerWithIdentifier("QuestionViewController") as QuestionViewController
        questionVC.questions = questions
        questionVC.index = index
        return questionVC
    }
    
    class func searchResults(searchText: String!) -> UIViewController {
        var searchResultsScreens: UIStoryboard = UIStoryboard(name:"SearchResults", bundle:nil)
        var searchResultsVC = searchResultsScreens.instantiateViewControllerWithIdentifier("SearchResultsViewController") as SearchResultsViewController
        searchResultsVC.searchText = searchText
        return searchResultsVC
    }
    
    class func openMapWithCoordinates(lat:Double, lng:Double, location: Bar, addressDictionary: [NSObject: AnyObject]) {
        
        var coordinate = CLLocationCoordinate2DMake(lat, lng)
        
        var placemark:MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:addressDictionary)
        
        var mapItem:MKMapItem = MKMapItem(placemark: placemark)
        mapItem.name = location.name
        mapItem.phoneNumber = location.phone_number
        
        let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey)
        
        var currentLocationMapItem:MKMapItem = MKMapItem.mapItemForCurrentLocation()
        
        MKMapItem.openMapsWithItems([currentLocationMapItem, mapItem], launchOptions: launchOptions)
    }
}
