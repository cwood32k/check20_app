//
//  PromoTableViewCell.swift
//  check20_app
//
//  Created by Chris Wood on 2/12/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class PromoTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selectionStyle = .None
    }
}
