//
//  LoginViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/11/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

let TWITTER_CONSUMER_KEY = "njosdSplr7R8Dma0TD8AXRksf"
let TWITTER_CONSUMER_SECRET = "UWnF1A90enLip9Jgod1XNFeqZAU9ax3MvIsQMJVaVQGu9BN8Bg"


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    @IBOutlet var loginFailedLabel: UILabel!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))

    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.borderWidth = 2.0
        loginButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        loadActivityIndicator()
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func validEmail(email:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)!
        return emailTest.evaluateWithObject(email)
    }
    
    func validateFields() -> Bool {
        if !validEmail(nameField.text) {
            loginFailedLabel.text = "Invalid email address"
            return false
        }
        
        if countElements(passwordField.text) < 6 {
            loginFailedLabel.text = "Invalid email or password"
            return false
        }
        
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        nameField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        loginFailedLabel.text = ""
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == nameField) {
            passwordField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func login(sender: UIButton) {
        nameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        nameField.text = nameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordField.text = passwordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if self.validateFields() {
            actInd.startAnimating()
            
            RKObjectManager.sharedManager().postObject(User(), path: Constants.API_LOGIN_PATH, parameters: ["email":nameField.text, "password":passwordField.text],
                success: { operation, mappingResult in
                    
                    var user: User = mappingResult.array()[0] as User
                    
                    if (user.auth_token != nil)
                    {
                        println("user logged in: \(user.username!)")
                        
                        self.actInd.stopAnimating()
                        
                        RKObjectManager.sharedManager().HTTPClient.setDefaultHeader("Authorization", value: user.auth_token)
                        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                        var user_data = NSKeyedArchiver.archivedDataWithRootObject(user)
                        defaults.setObject(user_data, forKey:Constants.USER_KEY) 
                        defaults.synchronize()
                        NotificationsManager.registerDevice()
                        
                        var home: MainNavigationViewController = MainNavigationViewController()
                        self.navigationController?.presentViewController(home, animated: true, completion: nil)
                    }
                    
                    
                }, failure: { operation, error in
                    self.actInd.stopAnimating()
                    
                    if let message = error.localizedRecoverySuggestion {
                        self.loginFailedLabel.text = message
                    } else {
                        self.loginFailedLabel.text = "An error occurred. Check network connection"
                    }
                }
            )
        }
        
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
