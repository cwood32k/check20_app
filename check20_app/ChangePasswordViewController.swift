
//
//  ChangePasswordViewController.swift
//  check20_app
//
//  Created by Chris Wood on 3/2/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class ChangePasswordViewController : UIViewController {
    @IBOutlet var navButton: UIButton!
    @IBOutlet var currentPasswordField: UITextField!
    @IBOutlet var newPasswordField: UITextField!
    @IBOutlet var newPasswordConfirmField: UITextField!
    @IBOutlet var updatedLabel: UILabel!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var updatedLabelTopSpace: NSLayoutConstraint!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadActivityIndicator()
        setStyles()
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func hideKeyboard() {
        currentPasswordField.resignFirstResponder()
        newPasswordField.resignFirstResponder()
        newPasswordConfirmField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        currentPasswordField.layer.borderWidth = 0
        newPasswordField.layer.borderWidth = 0
        newPasswordConfirmField.layer.borderWidth = 0
        errorLabel.text = ""
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        hideKeyboard()
    }
    
    func validateFields() -> Bool {
        hideKeyboard()
        if countElements(newPasswordField.text) < 6 {
            newPasswordField.layer.borderWidth = 1.0
            errorLabel.text = "Password must be > 6 characters"
            return false
        }
        if newPasswordField.text != newPasswordConfirmField.text {
            newPasswordField.layer.borderWidth = 1.0
            newPasswordConfirmField.layer.borderWidth = 1.0
            errorLabel.text = "New password does not match confirmation"
            return false
        }
        return true
    }
    
    @IBAction func setNewPassword(sender: UIButton) {
        if validateFields() {
            actInd.startAnimating()
            var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
            var user_id = (NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User).id
            
            var currentPassword = currentPasswordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            var newPassword = newPasswordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            var newPasswordConfirm = newPasswordConfirmField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            var user = User()
            user.password = newPassword
            user.password_confirmation = newPasswordConfirm
            
            RKObjectManager.sharedManager().putObject(user, path: "\(Constants.API_USERS_PATH)/\(user_id)", parameters: ["current_password": currentPassword],
                success: { operation, mappingResult in
                    
                    var user: User = mappingResult.array()[0] as User
                    if (user.auth_token != nil)
                    {
                        self.updatedLabelTopSpace.constant = self.updatedLabelTopSpace.constant + self.updatedLabel.frame.height
                        self.view.layoutIfNeeded()
                        UIView.animateWithDuration(2.5) {
                            self.updatedLabelTopSpace.constant = self.updatedLabelTopSpace.constant - self.updatedLabel.frame.height
                            self.view.layoutIfNeeded()
                        }
                        self.actInd.stopAnimating()
                    }
                },
                failure: { operation, error in
                    var jsonStr = error.localizedRecoverySuggestion!
                    var data = jsonStr.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: false)
                    var localError: NSError?
                    var json = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: &localError) as [String: AnyObject]
                    
                    if json["password"] != nil {
                        self.errorLabel.text = json["password"] as? String
                        self.currentPasswordField.layer.borderWidth = 1.0
                    } else {
                        
                    }
                    
                    self.actInd.stopAnimating()
                    
                    println("user update failed with errors: \(error.localizedRecoverySuggestion!)")
                }
            )
        }
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
