//
//  SearchResultsViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/26/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class SearchResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var navButton: UIButton!
    @IBOutlet var headerRightButton: UIButton!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchField: UITextField!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    @IBOutlet var resultsTableView: UITableView!
    var searchText: String!
    var bar_results = [SearchResult]()
    var promo_results = [SearchResult]()
    var imageCache = [String : UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadActivityIndicator()
        
        performSearch()
        setStyles()
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if !searchView.hidden {
            searchField.resignFirstResponder()
            toggleSearchView(false)
        }
    }
    
    func toggleSearchView(show: Bool) {
        UIView.transitionWithView(searchView, duration: 0.35, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.searchView.hidden = !show
            if self.searchView.hidden {
                self.searchField.resignFirstResponder()
                self.searchField.text = ""
            }
            }, completion:nil )
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        headerRightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func performSearch() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath(Constants.API_SEARCH_PATH, parameters: ["text":searchText],
            success: { operation, mappingResult in
                
                var search_results = mappingResult.array() as [SearchResult]
                if search_results.count > 0 {
                    self.bar_results = search_results.filter({$0.type == "Bar"})
                    self.promo_results = search_results.filter({$0.type == "Promo"})
                    self.resultsTableView.reloadData()
                    self.actInd.stopAnimating()
                } else {
                    var noResultsLabel = UILabel()
                    noResultsLabel.font = UIFont(name:"Roboto-Regular", size:18)
                    noResultsLabel.text = "No results for your search."
                    noResultsLabel.sizeToFit()
                    noResultsLabel.center = self.view.center
                    noResultsLabel.textColor = UIColor.whiteColor()
                    self.view.addSubview(noResultsLabel)
                    self.view.bringSubviewToFront(noResultsLabel)
                    self.actInd.stopAnimating()
                }
            }, failure: { operation, error in
                println("an error occured: \(error)")
            }
        )
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
            case 0:
                if bar_results.count == 0 { return 0 }
            case 1:
                if promo_results.count == 0 { return 0 }
            default:
                break
        }
        return 40
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as TableViewHeader
        
        switch (section) {
        case 0:
            if bar_results.count == 0 { return nil }
            headerCell.titleLabel.text = "Bars";
        case 1:
            if promo_results.count == 0 { return nil }
            headerCell.titleLabel.text = "Promos";
        default:
            headerCell.titleLabel.text = "Other";
        }
        headerCell.setStyles()
        
        return headerCell
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return bar_results.count
        case 1:
            return promo_results.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("SearchResultCell") as SearchResultsTableViewCell
        
        let result = indexPath.section == 0 ? bar_results[indexPath.row] as SearchResult : promo_results[indexPath.row] as SearchResult
        
        var image: UIImage? = imageCache[result.image_path]
        
        if image == nil {
            let result_image_url_string = Constants.ENV == "development" ? Constants.API_HOST + result.image_path : result.image_path
            let url: NSURL = NSURL(string:result_image_url_string)!
            let request: NSURLRequest = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if error == nil {
                    self.imageCache[result.image_path] = UIImage(data: data)!
                    dispatch_async(dispatch_get_main_queue(), {
                        cell.resultImageView.image = self.imageCache[result.image_path]
                    })
                }
            })
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                cell.resultImageView.image = image!
            })
        }
        
        cell.titleLabel.text = result.title
        cell.infoLabel.text = result.info
        
        cell.setStyles()
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as SearchResultsTableViewCell).setBackgroundImage()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath) as SearchResultsTableViewCell
        
        UIView.transitionWithView(cell, duration: 0.001, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            cell.titleLabel.textColor = Constants.CHECK20_LIGHT_ORANGE
            cell.infoLabel.textColor = Constants.CHECK20_LIGHT_ORANGE
            }, completion: { _ in
                UIView.transitionWithView(cell, duration: 0.55, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    cell.titleLabel.textColor = UIColor.whiteColor()
                    cell.infoLabel.textColor = UIColor.whiteColor()
                    }, completion: nil)
        })
        
        switch indexPath.section {
        case 0:
            var result = bar_results[indexPath.row]
            var bar = JSONParser.barFromDictionaryString(result.bar_data!)
            self.navigationController?.pushViewController(Navigation.bar(&bar), animated: true)
        case 1:
            var result = promo_results[indexPath.row]
            var promo = JSONParser.promoFromDictionaryString(result.promo_data!)
            self.navigationController?.pushViewController(Navigation.promo(promo, image:imageCache[result.image_path]), animated: true)
        default:
            break
        }
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        toggleSearchView(searchView.hidden)
        searchField.becomeFirstResponder()
    }
    
    @IBAction func search(sender: UIButton) {
        let searchText = searchField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if countElements(searchText) > 0 {
            self.navigationController?.pushViewController(Navigation.searchResults(searchText), animated:true)
        } else {
            
        }
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func cancelSearch(sender: UIButton) {
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
