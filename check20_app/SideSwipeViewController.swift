//
//  SideSwipeViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/29/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class SideSwipeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: SideSwipeTableViewCell = tableView.dequeueReusableCellWithIdentifier("SideSwipeCell") as SideSwipeTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        switch indexPath.row
        {
        case 0:
            cell.label.text = "Home"
            cell.icon.image = UIImage(named: "home_icon")
            cell.icon.frame.size.width += 14
        case 1:
            cell.label.text = "Map"
            cell.icon.image = UIImage(named: "map_icon")
        case 2:
            cell.label.text = "My Places"
            cell.icon.image = UIImage(named: "places_icon")
        case 3:
            cell.label.text = "Top & Trending"
            cell.icon.image = UIImage(named: "trending_icon")
        case 4:
            cell.label.text = "My Profile"
            cell.icon.image = UIImage(named: "profile_icon")
        case 5:
            cell.label.text = "Promos"
            cell.icon.image = UIImage(named: "promo_icon")
        case 6:
            cell.label.text = "Questions"
            cell.icon.image = UIImage(named: "q_icon")
        default:
            break
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var controller = self.navigationController?.topViewController as MMDrawerController
        var cell = tableView.cellForRowAtIndexPath(indexPath) as SideSwipeTableViewCell
        switch indexPath.row
        {
        case 0:
            //home
            if controller.centerViewController.isKindOfClass(HomeViewController) {
                showHighlight(cell)
                controller.closeDrawerAnimated(true, completion: nil)
            } else {
                showHighlight(cell)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }
            break
        case 1:
            //map
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.maps(), animated: true)
            break
        case 2:
            //my places
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.myPlaces(), animated: true)
            break
        case 3:
            //top and trending
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.topTrending(), animated: true)
            break
        case 4:
            //my profile
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.myProfile(), animated: true)
            break
        case 5:
            //promos
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.promos(), animated: true)
            break
        case 6:
            showHighlight(cell)
            self.navigationController?.pushViewController(Navigation.questions(), animated:true)
            break
        default:
            break
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        controller.closeDrawerAnimated(true, completion: nil)
        
    }
    
    func showHighlight(cell: SideSwipeTableViewCell) {
        UIView.transitionWithView(cell, duration: 0.001, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            cell.label.textColor = Constants.CHECK20_LIGHT_ORANGE
            }, completion: { _ in
                UIView.transitionWithView(cell, duration: 0.55, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    cell.label.textColor = UIColor.whiteColor()
                    }, completion: nil)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
