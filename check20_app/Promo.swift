//
//  Promo.swift
//  check20_app
//
//  Created by Chris Wood on 2/7/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class Promo : NSObject {
    var id: NSNumber!
    var bar_name: String!
    var bar_id: NSNumber!
    var name: String!
    var category: String!
    var desc: String!
    var image_path: String!
    var coupon_image_path: String!
    var start_date_relative: String!
    var end_date_relative: String!
    
    override init()
    {
        super.init()
    }
    
    init (values: [String: AnyObject]) {
        super.init()
        
        bar_name = values["bar_name"] as String
        bar_id = values["bar_id"] as NSNumber
        name = values["name"] as String
        category = values["category"] as String
        desc = values["description"] as String
        image_path = values["image_path"] as String
        coupon_image_path = values["coupon_image_path"] as String
        start_date_relative = values["start_date_relative"] as String
        end_date_relative = values["end_date_relative"] as String
    }

}