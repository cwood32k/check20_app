//
//  SideSwipeTableViewCell.swift
//  check20_app
//
//  Created by Chris Wood on 10/29/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class SideSwipeTableViewCell: UITableViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
