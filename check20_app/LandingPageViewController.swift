//
//  LandingPageViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/19/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class LandingPageViewController: UIViewController {

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signInLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if ((defaults.objectForKey(Constants.USER_KEY)) != nil) {
            var home: MainNavigationViewController = MainNavigationViewController()
            self.navigationController?.presentViewController(home, animated: true, completion: nil)
        } else {
            navigationController?.navigationBarHidden = true
            registerButton.layer.borderWidth = 2.0
            registerButton.layer.borderColor = UIColor.whiteColor().CGColor
            signInButton.layer.borderWidth = 2.0
            signInButton.layer.borderColor = UIColor.whiteColor().CGColor
        }
    }
    
    @IBAction func registerPressed(sender:UIButton) {
        let registerScreen: RegisterViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterViewController") as RegisterViewController
        navigationController?.pushViewController(registerScreen, animated: true)
    }
    
    @IBAction func signInPressed(sender:UIButton) {
        let loginScreen: LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
        navigationController?.pushViewController(loginScreen, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
