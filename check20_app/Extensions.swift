//
//  Extensions.swift
//  check20_app
//
//  Created by Chris Wood on 10/19/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

extension UILabel {
    override public func awakeFromNib() {
        var size = self.font.pointSize
        self.font = UIFont(name:"Roboto-Regular", size:size)
    }
}

extension UIButton {
    public override func awakeFromNib() {
        var size = self.titleLabel!.font.pointSize
        self.titleLabel!.font = UIFont(name:"Roboto-Bold", size:size)
    }
}

extension UITextView {
    public override func awakeFromNib() {
        var size = font.pointSize
        font = UIFont(name:"Roboto-Thin", size:size)
    }
}

extension UITextField {
    public override func awakeFromNib() {
        var size = font.pointSize
        font = UIFont(name:"Roboto-Regular", size:size)
    }
}

extension UIFont {
    public func sizeOfString (string: String, constrainedToHeight height: Double) -> CGSize {
        return NSString(string: string).boundingRectWithSize(CGSize(width: DBL_MAX, height: height),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: self],
            context: nil).size
    }
}

extension UIViewController {
    func shouldAutorotate() -> Bool {
        return true
    }
    
    func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
    }
}
// Usage example:
//    let deviceType : DeviceTypes = UIDevice().deviceType
//    let deviceName : String = deviceType.rawValue

public enum DeviceTypes : String {
    case simulator      = "Simulator",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5c       = "iPhone 5c",
    iPad4          = "iPad 4",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    unrecognized   = "?unrecognized?"
}

public extension UIDevice {
    public var deviceType: DeviceTypes {
        var sysinfo : [CChar] = Array(count: sizeof(utsname), repeatedValue: 0)
        let modelCode = sysinfo.withUnsafeMutableBufferPointer {
            (inout ptr: UnsafeMutableBufferPointer<CChar>) -> DeviceTypes in
            uname(UnsafeMutablePointer<utsname>(ptr.baseAddress))
            // skip 1st 4 256 byte sysinfo result fields to get "machine" field
            let machinePtr = advance(ptr.baseAddress, Int(_SYS_NAMELEN * 4))
            var modelMap : [ String : DeviceTypes ] = [
                "i386"      : .simulator,
                "x86_64"    : .simulator,
                "iPad2,1"   : .iPad2,          //
                "iPad3,1"   : .iPad3,          // (3rd Generation)
                "iPhone3,1" : .iPhone4,        //
                "iPhone3,2" : .iPhone4,        //
                "iPhone4,1" : .iPhone4S,       //
                "iPhone5,1" : .iPhone5,        // (model A1428, AT&T/Canada)
                "iPhone5,2" : .iPhone5,        // (model A1429, everything else)
                "iPad3,4"   : .iPad4,          // (4th Generation)
                "iPad2,5"   : .iPadMini1,      // (Original)
                "iPhone5,3" : .iPhone5c,       // (model A1456, A1532 | GSM)
                "iPhone5,4" : .iPhone5c,       // (model A1507, A1516, A1526 (China), A1529 | Global)
                "iPhone6,1" : .iPhone5S,       // (model A1433, A1533 | GSM)
                "iPhone6,2" : .iPhone5S,       // (model A1457, A1518, A1528 (China), A1530 | Global)
                "iPad4,1"   : .iPadAir1,       // 5th Generation iPad (iPad Air) - Wifi
                "iPad4,2"   : .iPadAir2,       // 5th Generation iPad (iPad Air) - Cellular
                "iPad4,4"   : .iPadMini2,      // (2nd Generation iPad Mini - Wifi)
                "iPad4,5"   : .iPadMini2,      // (2nd Generation iPad Mini - Cellular)
                "iPhone7,1" : .iPhone6plus,    // All iPhone 6 Plus's
                "iPhone7,2" : .iPhone6         // All iPhone 6's
            ]
            if let model = modelMap[String.fromCString(machinePtr)!] {
                return model
            }
            return DeviceTypes.unrecognized
        }
        return modelCode
    }
}

//date extensions

extension Int {
    var days: NSTimeInterval {
        let DAY_IN_SECONDS = 60 * 60 * 24
        var days:Double = Double(DAY_IN_SECONDS) * Double(self)
        return days
    }
    
    var seconds: TimeInterval {
        return TimeInterval(interval: self, unit: TimeIntervalUnit.Seconds)
    }
    
    var minutes: TimeInterval {
        return TimeInterval(interval: self, unit: TimeIntervalUnit.Minutes)
    }
    
    var hours: TimeInterval {
        return TimeInterval(interval: self, unit: TimeIntervalUnit.Hours)
    }
    
    var months: TimeInterval {
        return TimeInterval(interval: self, unit: TimeIntervalUnit.Months)
    }
    
    var years: TimeInterval {
        return TimeInterval(interval: self, unit: TimeIntervalUnit.Years)
    }
}

enum TimeIntervalUnit {
    case Seconds, Minutes, Hours, Days, Months, Years
    
    func dateComponents(interval: Int) -> NSDateComponents {
        var components: NSDateComponents = NSDateComponents()
        
        switch (self) {
        case .Seconds:
            components.second = interval
        case .Minutes:
            components.minute = interval
        case .Hours:
            components.day = interval
        case .Months:
            components.month = interval
        case .Years:
            components.year = interval
        default:
            components.day = interval
        }
        
        return components
    }
}

struct TimeInterval {
    var interval: Int
    var unit: TimeIntervalUnit
    
    init(interval: Int, unit: TimeIntervalUnit) {
        self.interval = interval
        self.unit = unit
    }
    
    var ago: NSDate {
        var calendar = NSCalendar.currentCalendar()
        let today = NSDate()
        var components = unit.dateComponents(-self.interval)
        return calendar.dateByAddingComponents(components, toDate: today, options:nil)!
    }
}

extension NSDate {
    func toString(let format: String = "MM/dd/yyyy") -> String? {
        var formatter: NSDateFormatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        formatter.dateFormat = format
        return formatter.stringFromDate(self)
    }
}

extension String {
    func toDate(let format: String = "MM/dd/yyyy") -> NSDate? {
        var formatter: NSDateFormatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        formatter.timeZone = NSTimeZone()
        formatter.dateFormat = format
        return formatter.dateFromString(self)
    }
}

func - (let left:NSDate, let right:TimeInterval) -> NSDate {
    var calendar = NSCalendar.currentCalendar()
    var components = right.unit.dateComponents(-right.interval)
    return calendar.dateByAddingComponents(components, toDate: left, options: nil)!
}

func < (let left: NSDate, let right: NSDate) -> Bool {
    var result: NSComparisonResult = left.compare(right)
    return result == NSComparisonResult.OrderedAscending
}

// End date extension
