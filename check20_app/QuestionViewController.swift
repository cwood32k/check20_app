//
//  QuestionsViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/15/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class QuestionViewController : UIViewController {
    
    @IBOutlet var navButton: UIButton!
    
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var questionView: UITextView!
    @IBOutlet var answer1View: UITextView!
    @IBOutlet var answer2View: UITextView!
    @IBOutlet var answer3View: UITextView!
    @IBOutlet var answer4View: UITextView!
    
    @IBOutlet var answer1Bubble: UIButton!
    @IBOutlet var answer2Bubble: UIButton!
    @IBOutlet var answer3Bubble: UIButton!
    @IBOutlet var answer4Bubble: UIButton!
    
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var savedLabel: UILabel!
    
    var questions: [Question]!
    var index: Int!
    
    var selected_choice: NSNumber?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var question: Question = questions[index]
        
        setStyles()
        
        setBubbleStyles()
        
        showQuestion()
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        numberLabel.font = UIFont(name:"Roboto-Bold", size:14)
        questionView.font = UIFont(name:"Roboto-Regular", size:18)
        answer1View.font = UIFont(name:"Roboto-Regular", size:16)
        answer2View.font = UIFont(name:"Roboto-Regular", size:16)
        answer3View.font = UIFont(name:"Roboto-Regular", size:16)
        answer4View.font = UIFont(name:"Roboto-Regular", size:16)
    }
    
    func setBubbleStyles() {
        answer1Bubble.layer.borderColor = UIColor.whiteColor().CGColor
        answer2Bubble.layer.borderColor = UIColor.whiteColor().CGColor
        answer3Bubble.layer.borderColor = UIColor.whiteColor().CGColor
        answer4Bubble.layer.borderColor = UIColor.whiteColor().CGColor
        answer1Bubble.layer.borderWidth = 2
        answer2Bubble.layer.borderWidth = 2
        answer3Bubble.layer.borderWidth = 2
        answer4Bubble.layer.borderWidth = 2
    }
    
    func showQuestion() {
        var question: Question = questions[index]
        numberLabel.text = "Question \(question.number)."
        questionView.text = question.question
        answer1View.text = question.answer1
        answer2View.text = question.answer2
        answer3View.text = question.answer3
        answer4View.text = question.answer4
        
        
        if let choice = question.user_choice {
            selected_choice = choice
            setBubbles(choice)
        }
    }
    
    func setBubbles(choice: NSNumber) {
        answer1Bubble.backgroundColor = Constants.CHECK20_DARK_GREY
        answer2Bubble.backgroundColor = Constants.CHECK20_DARK_GREY
        answer3Bubble.backgroundColor = Constants.CHECK20_DARK_GREY
        answer4Bubble.backgroundColor = Constants.CHECK20_DARK_GREY
        switch choice {
        case 1:
            answer1Bubble.backgroundColor = Constants.CHECK20_DARK_ORANGE
        case 2:
            answer2Bubble.backgroundColor = Constants.CHECK20_DARK_ORANGE
        case 3:
            answer3Bubble.backgroundColor = Constants.CHECK20_DARK_ORANGE
        case 4:
            answer4Bubble.backgroundColor = Constants.CHECK20_DARK_ORANGE
        default:
            break
        }
    }
    
    func showSavedMessage() {
        self.savedLabel.frame = CGRect(x: 0,y: 462, width: self.savedLabel.frame.size.width, height: self.savedLabel.frame.size.height)
        self.savedLabel.hidden = false
        UIView.animateWithDuration(2.5) {
            self.savedLabel.frame = CGRect(x: 0,y: 483,width: self.savedLabel.frame.size.width, height: self.savedLabel.frame.size.height)
        }
    }
    
    @IBAction func submitAnswer() {
        var question: Question = questions[index]
        let objectManager = RKObjectManager.sharedManager()
        var answer = Answer()
        answer.question_num = question.number
        answer.choice = selected_choice
        objectManager.postObject(answer, path: Constants.API_SAVE_ANSWER_PATH, parameters:nil,
            success: { operation, mappingResult in
                self.questions[self.index].user_choice = answer.choice
                self.showSavedMessage()
            }, failure: { operation, error in
                println("save answer failed with errors: \(error.localizedRecoverySuggestion!)")
            }
        )
    }
    
    @IBAction func exitPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func choiceSelected(sender: UIButton!) {
        submitButton.enabled = true
        setBubbles(sender.tag)
        switch sender.tag
        {
        case 1:
            selected_choice = 1
        case 2:
            selected_choice = 2
        case 3:
            selected_choice = 3
        case 4:
            selected_choice = 4
        default:
            selected_choice = nil
        }
    }
    
}
