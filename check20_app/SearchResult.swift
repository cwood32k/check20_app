//
//  SearchResult.swift
//  check20_app
//
//  Created by Chris Wood on 3/1/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class SearchResult: NSObject {
    var type: String!
    var id: NSNumber!
    var image_path: String!
    var title: String!
    var info: String!

    var bar_data: String?
    var promo_data: String?
    
    
}
