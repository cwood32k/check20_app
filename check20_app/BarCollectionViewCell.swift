//
//  BarCollectionViewCell.swift
//  check20_app
//
//  Created by Chris Wood on 10/22/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class BarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var bar_followers: UILabel!
    @IBOutlet var bar_headcount: UILabel!
    @IBOutlet var bar_likes: UILabel!
    @IBOutlet var bar_name: UILabel!
    
    var imageView: UIImageView!
    
    var imageOffset: CGPoint!
    
    var image: UIImage! {
        get {
            return self.image
        }
        set {
            self.imageView.image = newValue
            
            if imageOffset != nil {
                setImageOffset(imageOffset)
            } else {
                setImageOffset(CGPoint(x:0, y:0))
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        var contentViewIsAutoresized: Bool = CGSizeEqualToSize(self.frame.size, self.contentView.frame.size);
        
        if( !contentViewIsAutoresized) {
            var contentViewFrame: CGRect = self.contentView.frame;
            contentViewFrame.size = self.frame.size;
            self.contentView.frame = contentViewFrame;
        }
    }
    
    func setupImageView(frame: CGRect) {
        if imageView == nil {
            self.clipsToBounds = true
            var width: CGFloat = self.bounds.size.width
            var height: CGFloat = self.bounds.size.height
            imageView = UIImageView(frame: frame)
            imageView.contentMode = .ScaleAspectFill
            imageView.clipsToBounds = true
            self.addSubview(imageView)
            self.sendSubviewToBack(imageView)
        }
    }
    
    func setImageOffset(imageOffset: CGPoint) {
        self.imageOffset = imageOffset
        let frame: CGRect = imageView.bounds
        let offsetFrame: CGRect = CGRectOffset(frame, self.imageOffset.x, self.imageOffset.y)
        imageView.frame = offsetFrame
    }
}
