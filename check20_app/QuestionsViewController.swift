//
//  ReviewQuestionsViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/20/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class QuestionsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    @IBOutlet var navButton: UIButton!
    
    @IBOutlet var numberAnsweredLabel: UILabel!
    @IBOutlet var questionsTableView: UITableView!
    
    var presented_question_index = 0
    var pageView: UIPageViewController?
    
    var page_shown = false
    var questions = [Question]()
    var number_answered = 0
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionsTableView.backgroundView = nil
        questionsTableView.backgroundColor = Constants.CHECK20_LIGHT_GREY
        
        loadActivityIndicator()
        actInd.startAnimating()
        
        loadQuestions()
        
        setStyles()
    }
    
    override func viewDidAppear(animated: Bool) {
        if page_shown {
            questionsTableView.reloadData()
            setNumberAnsweredLabel()
        }
        page_shown = true
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func setNumberAnsweredLabel() {
        self.numberAnsweredLabel.font = UIFont(name:"Roboto-Bold", size:16)
        self.number_answered = self.questions.filter { $0.user_choice != nil }.count
        self.numberAnsweredLabel.text = "\(self.number_answered)/\(self.questions.count) Answered"
    }
    
    func loadQuestions() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath(Constants.API_QUESTIONS_PATH, parameters: nil,
            success: { operation, mappingResult in
                
                self.questions = mappingResult.array() as [Question]
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.questionsTableView.reloadData()
                    self.setNumberAnsweredLabel()
                })
            }, failure: { operation, error in
                println("an error occured: \(error)")
            }
        )
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.questionsTableView.dequeueReusableCellWithIdentifier("QuestionTableViewCell") as QuestionTableViewCell
        
        cell.titleLabel.text = questions[indexPath.row].question
        
        if questions[indexPath.row].user_choice != nil {
            cell.answeredStar.image = UIImage(named: "star_orange")
        } else {
            cell.answeredStar.image = UIImage(named: "star_grey")
        }
        
        var lastIndex = tableView.indexPathsForVisibleRows()!.last as NSIndexPath
        if indexPath.row == lastIndex.row {
            self.actInd.stopAnimating()
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as QuestionTableViewCell).setBackgroundImage()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath) as QuestionTableViewCell
        
        UIView.transitionWithView(cell.titleLabel, duration: 0.001, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                cell.titleLabel.textColor = Constants.CHECK20_LIGHT_ORANGE
            }, completion: { _ in
                UIView.transitionWithView(cell.titleLabel, duration: 0.55, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    cell.titleLabel.textColor = UIColor.whiteColor()
                }, completion: nil)
        })
        presented_question_index = indexPath.row
        presentQuestionPageViewController()
    }
    
    func presentQuestionPageViewController() {
        pageView = UIPageViewController(transitionStyle: .PageCurl, navigationOrientation: .Horizontal, options: nil)
        pageView!.delegate = self
        pageView!.dataSource = self
        
        pageView!.setViewControllers([Navigation.question(&questions, index: presented_question_index)], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion:nil)
        
        self.navigationController?.pushViewController(pageView!, animated: true)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if (presented_question_index > 0) {
            return Navigation.question(&questions, index: presented_question_index - 1)
        } else {
            return Navigation.question(&questions, index: questions.count - 1)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if (presented_question_index < questions.count - 1) {
            return Navigation.question(&questions, index: presented_question_index + 1)
        } else {
            return Navigation.question(&questions, index: 0)
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject], transitionCompleted completed: Bool) {
        if completed {
            let current = pageViewController.viewControllers[0] as QuestionViewController
            presented_question_index = current.index
        }
    }
    
    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
}