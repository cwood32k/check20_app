//
//  MainNavigationViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/29/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class MainNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarHidden = true
        
        self.pushViewController(Navigation.home(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
