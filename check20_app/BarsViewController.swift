//
//  BarsViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/22/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class BarsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet var navButton: UIButton!
    @IBOutlet var headerRightButton: UIButton!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchField: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var tabs: UISegmentedControl!
    
    var bars = [Bar]()
    var page_shown = false
    var imageCache = [String : UIImage]()
    
    var refreshControl = UIRefreshControl()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadActivityIndicator()
        actInd.startAnimating()
        
        setupPullDownRefresh()
        
        loadBars()
        
        setStyles()
    }
    
    func setupPullDownRefresh() {
        refreshControl.tintColor = UIColor.grayColor()
        refreshControl.addTarget(self, action: "loadBars", forControlEvents: .ValueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
    }
    
    override func viewDidAppear(animated: Bool) {
        if page_shown {
            bars = bars.filter{$0.following}
            collectionView.reloadData()
        }
        page_shown = true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if !searchView.hidden {
            searchField.resignFirstResponder()
            toggleSearchView(false)
        }
    }
    
    func toggleSearchView(show: Bool) {
        UIView.transitionWithView(searchView, duration: 0.35, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.searchView.hidden = !show
            if self.searchView.hidden {
                self.searchField.resignFirstResponder()
                self.searchField.text = ""
            }
            }, completion:nil )
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        headerRightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    
    func loadBars() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath(Constants.API_MY_PLACES_PATH, parameters: nil,
            success: { operation, mappingResult in
                
                self.bars = mappingResult.array() as [Bar]
                
                if self.bars.count > 0 {
                    self.collectionView.reloadData()
                } else {
                    var noPlacesLabel = UILabel()
                    noPlacesLabel.font = UIFont(name:"Roboto-Regular", size:18)
                    noPlacesLabel.textAlignment = .Center
                    noPlacesLabel.frame = CGRectMake(0, 0, self.view.frame.size.width - 10, 48)
                    noPlacesLabel.numberOfLines = 0
                    noPlacesLabel.text = "You have no venues added to My Places. Search on the map for bars in your area, and add any that you would like to follow!"
                    noPlacesLabel.sizeToFit()
                    noPlacesLabel.center = self.view.center
                    noPlacesLabel.textColor = UIColor.whiteColor()
                    self.view.addSubview(noPlacesLabel)
                    self.view.bringSubviewToFront(noPlacesLabel)
                }
                
                self.actInd.stopAnimating()
                self.refreshControl.endRefreshing()
                
            }, failure: { operation, error in
                println("an error occured: \(error)")
            }
        )
    }
    
    
    //collection view delegate methods
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bars.count
    }
    
    func collectionView(collectionView:UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 10, height: collectionView.frame.size.height / 3.5)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: BarCollectionViewCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("BarCell", forIndexPath: indexPath) as BarCollectionViewCell
        cell.setupImageView(CGRect(x:0, y:0, width: collectionView.frame.size.width - 10, height: collectionView.frame.height / 3.5))
        
        let bar = bars[indexPath.row] as Bar
        
        var image: UIImage? = imageCache[bar.outside_image_path]
        
        if image == nil {
            let outside_image_url_string = Constants.ENV == "development" ? Constants.API_HOST + bar.outside_image_path! : bar.outside_image_path!
            let url: NSURL = NSURL(string:outside_image_url_string)!
            let request: NSURLRequest = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if error == nil {
                    image = UIImage(data: data)
                    
                    self.imageCache[bar.outside_image_path] = image!
                    dispatch_async(dispatch_get_main_queue(), {
                        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? BarCollectionViewCell {
                            var imageForCell: UIImage = image!
                            cell.image = imageForCell
                            let yOffset: CGFloat = ((collectionView.contentOffset.y - cell.frame.origin.y) / (cell.frame.height * 3)) * 15
                            cell.setImageOffset(CGPoint(x: 0, y: yOffset))
                        }
                    })
                    
                }
            })
        } else {
            var imageForCell: UIImage = image!
            cell.image = imageForCell
            let yOffset: CGFloat = ((collectionView.contentOffset.y - cell.frame.origin.y) / (cell.frame.height * 3)) * 15
            cell.setImageOffset(CGPoint(x: 0, y: yOffset))
        }
        
        cell.bar_name.text = bar.name
        cell.bar_name.sizeToFit()
        cell.bar_followers.text = "\(bar.female_followers) girls and \(bar.male_followers) guys following"
        cell.bar_headcount.text = "\(bar.female_check_ins) girls and \(bar.male_check_ins) guys checked in right now"
        cell.bar_likes.text = "\(bar.female_likes) girls and \(bar.male_likes) guys liked this right now"
             
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        navigationController?.pushViewController(Navigation.bar(&bars[indexPath.row]), animated: true)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        for view in collectionView.visibleCells() {
            var view:BarCollectionViewCell = view as BarCollectionViewCell
            var cyOffset: CGFloat = collectionView.contentOffset.y;
            let yOffset: CGFloat = ((collectionView.contentOffset.y - view.frame.origin.y) / (view.frame.height * 3)) * 15
            view.setImageOffset(CGPoint(x: 0, y: yOffset))
        }
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        toggleSearchView(searchView.hidden)
        searchField.becomeFirstResponder()
    }
    
    @IBAction func search(sender: UIButton) {
        let searchText = searchField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if countElements(searchText) > 0 {
            self.navigationController?.pushViewController(Navigation.searchResults(searchText), animated:true)
        } else {
            
        }
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func cancelSearch(sender: UIButton) {
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
