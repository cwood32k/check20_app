//
//  SearchResultsTableViewHeader.swift
//  check20_app
//
//  Created by Chris Wood on 3/1/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class TableViewHeader: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Constants.CHECK20_LIGHT_GREY
        selectionStyle = .None
    }
    
    func setStyles() {
        titleLabel.font = UIFont(name:"Roboto-Bold", size:18)
    }
}
