//
//  MyProfileViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/14/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class MyProfileViewController : UIViewController {
    @IBOutlet var navButton: UIButton!
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var dobField: UITextField!
    @IBOutlet var gender: UISegmentedControl!
    
    @IBOutlet var emailValidationField: UILabel!
    @IBOutlet var usernameValidationField: UILabel!
    
    @IBOutlet var editFieldsButton: UIButton!
    @IBOutlet var saveChangesButton: UIButton!
    @IBOutlet var cancelChangesButton: UIButton!
    
    @IBOutlet var updatedLabel: UILabel!
    @IBOutlet var updatedLabelTopSpace: NSLayoutConstraint!
    
    var user: User = User()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    override func viewDidLoad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        view.addGestureRecognizer(tap)
        
        let dobPicker: UIDatePicker = UIDatePicker()
        dobPicker.datePickerMode = .Date
        dobPicker.addTarget(self, action: Selector("dobPickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        dobPicker.maximumDate = 21.years.ago
        dobField.inputView = dobPicker
        
        setStyles()
        
        loadActivityIndicator()
        actInd.startAnimating()
    }
    
    override func viewWillAppear(animated: Bool) {
       getUser()
    }
    
    func getUser() {
        let objectManager = RKObjectManager.sharedManager()
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var user_id = (NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User).id
        objectManager.getObject(user, path: "\(Constants.API_USERS_PATH)/\(user_id)", parameters: nil,
            success: { operation, mappingResult in
                var user: User = mappingResult.array()[0] as User
                self.user = user
                self.emailField.text = user.email
                self.usernameField.text = user.username
                self.dobField.text = user.dob
                self.gender.selectedSegmentIndex = user.gender == "M" ? 0 : 1
                self.actInd.stopAnimating()
            }, failure: { operation, error in
                println("an error occured: \(error)")
        })
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func validEmail(email:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)!
        return emailTest.evaluateWithObject(email)
    }
    
    func validUsername(username:String) -> Bool {
        let usernameRegex = "^[A-Za-z0-9_]+$"
        var usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegex)!
        return usernameTest.evaluateWithObject(username)
    }
    
    func validateFields() -> Bool {
        if (!self.validEmail(emailField.text!)) {
            emailField.layer.borderWidth = 1.0
            emailValidationField.text = "Invalid email address"
            return false
        }
        if (!self.validUsername(usernameField.text!)) {
            usernameField.layer.borderWidth = 1.0
            usernameValidationField.text = "Letters, numbers, and underscores only"
            return false
        }
//        if countElements(passwordField.text!) < 6 {
//            return false
//        }
//        if passwordField.text != passwordConfirmField.text {
//            return false
//        }
        
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        hideKeyboard()
    }
    
    func hideKeyboard() {
        emailField.resignFirstResponder()
        usernameField.resignFirstResponder()
        dobField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == emailField {
            emailField.layer.borderWidth = 0
            emailValidationField.text = ""
        }
        
        if textField == usernameField {
            usernameField.layer.borderWidth = 0
            usernameValidationField.text = ""
        }
    }
    
    func dobPickerChanged(sender: UIDatePicker) {
        var picker: UIDatePicker = dobField.inputView as UIDatePicker
        dobField.text = picker.date.toString()
    }
    
    func toggleFieldsAndButtons() {
        editFieldsButton.hidden = !editFieldsButton.hidden
        cancelChangesButton.hidden = !cancelChangesButton.hidden
        saveChangesButton.hidden = !saveChangesButton.hidden
        emailField.enabled = !emailField.enabled
        usernameField.enabled = !usernameField.enabled
        //passwordConfirmField.enabled = false
        dobField.enabled = !dobField.enabled
        gender.enabled = !gender.enabled
    }
    
    @IBAction func editFields(sender: UIButton) {
        toggleFieldsAndButtons()
    }
    
    @IBAction func saveChanges(sender: UIButton!) {
        hideKeyboard()
        if validateFields() {
            actInd.startAnimating()
            var trimmedEmail = emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            var trimmedUsername = usernameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            //var trimmedPasswordConfirm = passwordConfirmField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            user.email = trimmedEmail
            user.username = trimmedUsername
            user.dob = (dobField.inputView as UIDatePicker).date.toString(format: "MM/dd/yyyy")
            user.gender = gender.selectedSegmentIndex == 0 ? "M" : "F"
            user.password = nil
            //user.password_confirmation = trimmedPasswordConfirm
            var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
            var user_id = (NSKeyedUnarchiver.unarchiveObjectWithData(defaults.objectForKey(Constants.USER_KEY) as NSData) as User).id
            RKObjectManager.sharedManager().putObject(user, path: "\(Constants.API_USERS_PATH)/\(user_id)", parameters: nil,
                success: { operation, mappingResult in
                    
                    var user: User = mappingResult.array()[0] as User
                    if (user.auth_token != nil)
                    {
                        self.updatedLabelTopSpace.constant = self.updatedLabelTopSpace.constant + self.updatedLabel.frame.height
                        self.view.layoutIfNeeded()
                        UIView.animateWithDuration(2.5) {
                            self.updatedLabelTopSpace.constant = self.updatedLabelTopSpace.constant - self.updatedLabel.frame.height
                            self.view.layoutIfNeeded()
                        }
                        self.toggleFieldsAndButtons()
                        self.actInd.stopAnimating()
                    }
                },
                failure: { operation, error in
                    var jsonStr = error.localizedRecoverySuggestion!
                    var data = jsonStr.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: false)
                    var localError: NSError?
                    var json = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: &localError) as [String: AnyObject]
                    
                    if json["email"] != nil {
                        self.emailValidationField.text = "email already taken"
                        self.emailField.layer.borderWidth = 1.0
                    } else {
                        self.emailValidationField.text = "An error occurred. Try again"
                    }
                    
                    self.actInd.stopAnimating()
                    
                    println("user update failed with errors: \(error.localizedRecoverySuggestion!)")
                }
            )
        }
    }
    
    @IBAction func cancelChanges(sender: UIButton!) {
        emailField.text = user.email
        usernameField.text = user.username
        dobField.text = user.dob
        gender.selectedSegmentIndex = user.gender == "M" ? 0 : 1
        
        toggleFieldsAndButtons()
    }
    
    @IBAction func changePassword(sender: UIButton) {
        var changePasswordVC = self.storyboard?.instantiateViewControllerWithIdentifier("ChangePasswordViewController") as ChangePasswordViewController
        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
    @IBAction func logout(sender: UIButton) {
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(Constants.USER_KEY)
        defaults.synchronize()
        
        NotificationsManager.registerDevice()
        
        var loginScreens: UIStoryboard = UIStoryboard(name: "LoginJoin", bundle: nil)
        var loginVC: LandingPageViewController = loginScreens.instantiateViewControllerWithIdentifier("LandingPageViewController") as LandingPageViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
}
