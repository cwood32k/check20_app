//
//  User.swift
//  check20_app
//
//  Created by Chris Wood on 10/11/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class User : NSObject {
    var email: String!
    var username: String!
    var auth_token: String!
    var errors: String?
    var dob: String!
    var gender: String!
    var num_answers: NSNumber!
    
    //the password fields are only used for sending on registration or change password
    var password: String?
    var password_confirmation: String?
    
    var id: NSNumber!
    
    required init(coder aDecoder: NSCoder) {
        email = aDecoder.decodeObjectForKey("email") as String
        username = aDecoder.decodeObjectForKey("username") as String
        auth_token = aDecoder.decodeObjectForKey("auth_token") as String
        errors = aDecoder.decodeObjectForKey("errors") as? String
        dob = aDecoder.decodeObjectForKey("dob") as String
        gender = aDecoder.decodeObjectForKey("gender") as String
        num_answers = aDecoder.decodeObjectForKey("num_answers") as NSNumber
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(email, forKey: "email")
        aCoder.encodeObject(username, forKey: "username")
        aCoder.encodeObject(errors, forKey: "errors")
        aCoder.encodeObject(dob, forKey: "dob")
        aCoder.encodeObject(gender, forKey: "gender")
        aCoder.encodeObject(num_answers, forKey: "num_answers")
        aCoder.encodeObject(auth_token, forKey: "auth_token")
    }
    
    override init()
    {
        super.init()
    }
}
