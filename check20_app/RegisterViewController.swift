//
//  RegisterViewController.swift
//  check20_app
//
//  Created by Chris Wood on 10/12/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var passwordConfirmField: UITextField!
    @IBOutlet var dobField: UITextField!
    @IBOutlet var gender: UISegmentedControl!
    
    @IBOutlet var emailValidationField: UILabel!
    @IBOutlet var usernameValidationField: UILabel!
    @IBOutlet var passwordValidationField: UILabel!
    @IBOutlet var passwordConfirmValidationField: UILabel!
    @IBOutlet var dobValidationField: UILabel!
    @IBOutlet var genderValidationField: UILabel!
    
    @IBOutlet var registerButton: UIButton!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideKeyboard")
        view.addGestureRecognizer(tap)
        
        let dobPicker: UIDatePicker = UIDatePicker()
        dobPicker.datePickerMode = .Date
        dobPicker.addTarget(self, action: Selector("dobPickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        dobPicker.maximumDate = 21.years.ago
        dobField.inputView = dobPicker
        
        registerButton.layer.borderWidth = 2.0
        registerButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        loadActivityIndicator()
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func dobPickerChanged(sender: UIDatePicker) {
        var picker: UIDatePicker = dobField.inputView as UIDatePicker
        dobField.text = picker.date.toString()
    }
    
    func validEmail(email:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)!
        return emailTest.evaluateWithObject(email)
    }
    
    func validUsername(username:String) -> Bool {
        let usernameRegex = "^[A-Za-z0-9_]+$"
        var usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegex)!
        return usernameTest.evaluateWithObject(username)
    }
    
    func validateFields() -> Bool {
        if (!self.validEmail(emailField.text!)) {
            emailField.layer.borderWidth = 1.0
            emailValidationField.text = "Invalid email address"
            return false
        }
        if (!self.validUsername(usernameField.text!)) {
            usernameField.layer.borderWidth = 1.0
            usernameValidationField.text = "Letters, numbers, and underscores only"
            return false
        }
        if countElements(passwordField.text!) < 6 {
            passwordField.layer.borderWidth = 1.0
            passwordValidationField.text = "Password must be > 6 characters"
            return false
        }
        if passwordField.text != passwordConfirmField.text {
            passwordField.layer.borderWidth = 1.0
            passwordConfirmField.layer.borderWidth = 1.0
            passwordConfirmValidationField.text = "Password does not match confirmation"
            return false
        }
        if dobField.text == "" {
            dobField.layer.borderWidth = 1.0
            dobValidationField.text = "Please select your birth date"
            return false
        }
        if gender.selectedSegmentIndex == UISegmentedControlNoSegment {
            genderValidationField.text = "Select your sex"
            return false
        }
        
        return true
    }
    
    func hideKeyboard() {
        emailField.resignFirstResponder()
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        passwordConfirmField.resignFirstResponder()
        dobField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == emailField {
            emailField.layer.borderWidth = 0
            emailValidationField.text = ""
        }
        if textField == usernameField {
            usernameField.layer.borderWidth = 0
            usernameValidationField.text = ""
        }
        if textField == passwordField || textField == passwordConfirmField {
            passwordField.layer.borderWidth = 0
            passwordValidationField.text = ""
            passwordConfirmField.layer.borderWidth = 0
            passwordConfirmValidationField.text = ""
        }
        if textField == dobField {
            dobField.layer.borderWidth = 0
            dobValidationField.text = ""
            dobField.text = (dobField.inputView as UIDatePicker).date.toString()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == emailField) {
            usernameField.becomeFirstResponder()
        }
        else if textField == usernameField {
            passwordField.becomeFirstResponder()
        }
        else if textField == passwordField {
            passwordConfirmField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    @IBAction func genderChanged() {
        genderValidationField.text = ""
    }
    
    @IBAction func register(sender:UIButton) {
        emailField.text = emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        usernameField.text = usernameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordField.text = passwordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordConfirmField.text = passwordConfirmField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if validateFields() {
            actInd.startAnimating()

            var user = User()
            user.email = emailField.text
            user.username = usernameField.text
            user.dob = (dobField.inputView as UIDatePicker).date.toString(format: "MM/dd/yyyy")
            user.password = passwordField.text
            user.password_confirmation = passwordConfirmField.text
            user.gender = gender.selectedSegmentIndex == 0 ? "M" : "F"
            
            RKObjectManager.sharedManager().postObject(user, path: Constants.API_REGISTER_PATH, parameters:nil,
                success: { operation, mappingResult in
                    
                    var user: User = mappingResult.array()[0] as User
                    if (user.auth_token != nil)
                    {
                        println("user registered: \(user.username)")
                        NotificationsManager.registerDevice()
                        
                        self.actInd.stopAnimating()
                        RKObjectManager.sharedManager().HTTPClient.setDefaultHeader("Authorization", value: user.auth_token)
                        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                        var user_data = NSKeyedArchiver.archivedDataWithRootObject(user)
                        defaults.setObject(user_data, forKey:Constants.USER_KEY)
                        defaults.synchronize()
                        var home: MainNavigationViewController = MainNavigationViewController()
                        self.navigationController?.presentViewController(home, animated: true, completion: nil)
                    }
                    
                },
                failure: { operation, error in
                    var jsonStr = error.localizedRecoverySuggestion!
                    var data = jsonStr.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: false)
                    var localError: NSError?
                    var json = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: &localError) as [String: AnyObject]
                    
                    if json["email"] != nil {
                        self.emailValidationField.text = "email already taken"
                        self.emailField.layer.borderWidth = 1.0
                    } else {
                        self.emailValidationField.text = "An error occurred. Try again"
                    }

                    println("registration failed with errors: \(error.localizedRecoverySuggestion!)")
                }
            )
        }
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
