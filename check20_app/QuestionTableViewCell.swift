//
//  QuestionTableViewCell.swift
//  check20_app
//
//  Created by Chris Wood on 2/20/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class QuestionTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var answeredStar: UIImageView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = Constants.CHECK20_LIGHT_GREY
        selectionStyle = .None
    }
    
    func setBackgroundImage() {
        var imageView = UIImageView(frame: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(4, 2, 0, 2)))
        
        imageView.image = UIImage(named: "cell_background_grey")
        addSubview(imageView)
        sendSubviewToBack(imageView)
    }
}