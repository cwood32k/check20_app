//
//  Bar.swift
//  check20_app
//
//  Created by Chris Wood on 10/11/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class Bar : NSObject {
    var address1: String!
    var address2: String!
    var phone_number: String!
    var website: String!
    var name: String!
    var avatar_path: String!
    var video_path: String!
    var video_thumb_path: String!
    var outside_image_path: String!
    var inside_image1_path: String!
    var inside_image2_path: String!
    var inside_image3_path: String!
    var tagline: String!
    var about_us: String!
    var type: String!
    var music_type: String!
    var age_demographic: String!
    var following: Bool = false
    var checked_in: Bool = false
    var liked: Bool = false
    var male_followers: NSNumber!
    var female_followers: NSNumber!
    var male_check_ins: NSNumber!
    var female_check_ins: NSNumber!
    var male_likes: NSNumber!
    var female_likes: NSNumber!
    
    var id: NSNumber!
    
    override init()
    {
        super.init()
    }
    
    init(values: [String: AnyObject]) {
        super.init()
        
        address1 = values["address1"] as String
        address2 = values["address2"] as String
        phone_number = values["phone_number"] as String
        website = values["website"] as String
        name = values["name"] as String
        avatar_path = values["avatar_path"] as String
        video_path = values["video_path"] as String
        video_thumb_path = values["video_thumb_path"] as String
        outside_image_path = values["outside_image_path"] as String
        inside_image1_path = values["inside_image1_path"] as String
        inside_image2_path = values["inside_image2_path"] as String
        inside_image3_path = values["inside_image3_path"] as String
        tagline = values["tagline"] as String
        about_us = values["about_us"] as String
        type = values["bar_type"] as String
        music_type = values["music_type"] as String
        age_demographic = values["age_demographic"] as String
        following = values["following"] as Bool
        checked_in = values["checked_in"] as Bool
        liked = values["liked"] as Bool
        male_followers = values["male_followers"] as NSNumber
        female_followers = values["female_followers"] as NSNumber
        male_check_ins = values["male_check_ins"] as NSNumber
        female_check_ins = values["female_check_ins"] as NSNumber
        male_likes = values["male_likes"] as NSNumber
        female_likes = values["female_likes"] as NSNumber
        id = values["id"] as NSNumber
        
    }
}
