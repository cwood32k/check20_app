//
//  Check20TextField.swift
//  check20_app
//
//  Created by Chris Wood on 10/19/14.
//  Copyright (c) 2014 check20. All rights reserved.
//

class Check20TextField : UITextField {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        backgroundColor = UIColor.clearColor()
        layer.borderWidth = 0.0
        layer.borderColor =  Constants.CHECK20_DARK_ORANGE.CGColor
        borderStyle = .None
        frame.size = CGSize(width:200, height:45)
        textColor = UIColor.whiteColor()
        font = UIFont(name:"Roboto-Regular", size:18)
    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 5, 5)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 5, 5)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        var layerFrame: CGRect  = CGRect(x:0, y:0, width:frame.size.width, height:frame.size.height)
//        var path: CGMutablePathRef  = CGPathCreateMutable()
//        CGPathMoveToPoint(path, nil, 0, layerFrame.size.height)
//        CGPathAddLineToPoint(path, nil, layerFrame.size.width, layerFrame.size.height) // bottom line
//        var line = CAShapeLayer()
//        line.path = path
//        line.lineWidth = 2
//        line.frame = layerFrame
//        line.strokeColor = UIColor.darkGrayColor().CGColor
//        layer.addSublayer(line)
    }
    
}


