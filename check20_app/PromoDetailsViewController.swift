//
//  PromoDetails.swift
//  check20_app
//
//  Created by Chris Wood on 2/16/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class PromoDetailsViewController: UIViewController {
    
    var promo: Promo!
    var promo_image: UIImage?
    
    var promo_id: NSNumber?
    var brand_new: Bool = false
    
    @IBOutlet var navButton: UIButton!
    
    var bar: Bar?
    @IBOutlet var bar_name: UIButton!
    @IBOutlet var promo_name: UILabel!
    @IBOutlet var promo_desc: UITextView!
    @IBOutlet var promo_imageView: UIImageView!
    @IBOutlet var promo_couponView: UIImageView!
    @IBOutlet var promo_start_date: UILabel!
    @IBOutlet var promo_end_date: UILabel!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let id = promo_id {
            loadActivityIndicator()
            actInd.startAnimating()
            promo = Promo()
            getPromo()
        } else {
            updateView()
            getBar()
        }
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func updateView() {
        if let image = promo_image {
            promo_imageView.image = image
            loadCoupon()
        } else {
            loadImage()
            loadCoupon()
        }
        
        bar_name.setTitle(promo.bar_name, forState: .Normal)
        promo_name.text = promo.name
        promo_desc.text = promo.desc
        promo_desc.sizeToFit()
        promo_start_date.text = promo.start_date_relative
        promo_end_date.text = promo.end_date_relative
        setStyles()
    }
    
    func setStyles() {
        bar_name.setTranslatesAutoresizingMaskIntoConstraints(false)
        var font: UIFont = UIFont(name:"Roboto-Bold", size:16)!
        var button_size: CGSize = font.sizeOfString(bar_name.titleLabel!.text!, constrainedToHeight: Double(bar_name.frame.height))
        var constW:NSLayoutConstraint = NSLayoutConstraint(item: bar_name, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: button_size.width + 20)
        self.view.addConstraint(constW)
        
        promo_name.font = UIFont(name:"Roboto-BlackItalic", size:14)
        promo_desc.font = UIFont(name:"Roboto-Thin", size:16)
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func getPromo() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObject(promo, path: "\(Constants.API_PROMOS_PATH)/\(promo_id!)", parameters: nil,
            success: { operation, mappingResult in
                self.promo = mappingResult.array()[0] as Promo
                self.updateView()
                self.actInd.stopAnimating()
                if self.brand_new {
                    UIView.animateWithDuration(1.5, animations: {
                        self.view.backgroundColor = Constants.CHECK20_DARK_ORANGE
                        self.view.backgroundColor = Constants.CHECK20_LIGHT_GREY
                    })
                }
                self.getBar()
            }, failure: { operation, error in
                println("an error occured: \(error)")
        })
    }
    
    func getBar() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObject(Bar(), path: "\(Constants.API_BARS_PATH)/\(promo.bar_id!)", parameters: nil,
            success: { operation, mappingResult in
                self.bar = mappingResult.array()[0] as? Bar
            }, failure: { operation, error in
                println("an error occured: \(error)")
        })
    }
    
    func loadImage() {
        let promo_image_url_string = Constants.ENV == "development" ? Constants.API_HOST + promo.image_path! : promo.image_path!
        let image_url: NSURL = NSURL(string:promo_image_url_string)!
        let image_request: NSURLRequest = NSURLRequest(URL: image_url)
        NSURLConnection.sendAsynchronousRequest(image_request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), {
                    self.promo_imageView.image = UIImage(data: data)
                })
            }
        })
    }
    
    func loadCoupon() {
        let coupon_image_url_string = Constants.ENV == "development" ? Constants.API_HOST + promo.coupon_image_path! : promo.coupon_image_path!
        let coupon_url: NSURL = NSURL(string:coupon_image_url_string)!
        let coupon_request: NSURLRequest = NSURLRequest(URL: coupon_url)
        NSURLConnection.sendAsynchronousRequest(coupon_request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), {
                    self.promo_couponView.image = UIImage(data: data)
                })
            }
        })
    }
    
    @IBAction func showBar(sender: UIButton) {
        if let bar = self.bar {
            self.navigationController?.pushViewController(Navigation.bar(bar), animated: true)
        }
    }
    
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}