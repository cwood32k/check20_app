//
//  RestKitConfig.swift
//  check20_app
//
//  Created by Chris Wood on 2/15/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class RestKitManager {
    
    class var getOrCreateSharedInstance: RestKitManager {
        struct Static {
            static var instance: RestKitManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = RestKitManager()
        }
        
        return Static.instance!
    }
    
    init() {
        let objectManager = RKObjectManager(baseURL: NSURL(string: Constants.API_HOST))
        objectManager.requestSerializationMIMEType = RKMIMETypeJSON
        
        let questionMapping = RKObjectMapping(forClass: Question.self)
        questionMapping.addAttributeMappingsFromDictionary([
            "number":"number",
            "question":"question",
            "answer1":"answer1",
            "answer2":"answer2",
            "answer3":"answer3",
            "answer4":"answer4",
            "user_choice":"user_choice"
            ])

        let questionResponseDescriptor = RKResponseDescriptor(mapping: questionMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_QUESTION_PATH, keyPath:"question", statusCodes: NSIndexSet(index:200))
        let nextQuestionResponseDescriptor = RKResponseDescriptor(mapping: questionMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_NEXT_QUESTION_PATH, keyPath:"question", statusCodes: NSIndexSet(index:200))
        let questionsResponseDescriptor = RKResponseDescriptor(mapping: questionMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_QUESTIONS_PATH, keyPath:"questions", statusCodes: NSIndexSet(index:200))
        
        let answerMapping = RKObjectMapping(forClass: Answer.self)
        answerMapping.addAttributeMappingsFromDictionary([
            "question_num":"question_num",
            "choice":"choice"
            ])
        let answerResponseMapping = RKObjectMapping(forClass: Answer.self)
        answerResponseMapping.addAttributeMappingsFromDictionary([
            "question_num":"question_num",
            "question_id":"question_id",
            "user_id":"user_id",
            "choice":"choice"
            ])
        
        let saveAnswerRequestDescriptor = RKRequestDescriptor(mapping: answerMapping.inverseMapping(), objectClass: Answer.self, rootKeyPath:"answer", method: RKRequestMethod.POST)
        let saveAnswerResponseDescriptor = RKResponseDescriptor(mapping: answerResponseMapping, method: RKRequestMethod.POST, pathPattern: Constants.API_SAVE_ANSWER_PATH, keyPath:"answer", statusCodes: NSIndexSet(index:201))
        
        objectManager.addResponseDescriptor(questionsResponseDescriptor)
        objectManager.addResponseDescriptor(questionResponseDescriptor)
        objectManager.addResponseDescriptor(nextQuestionResponseDescriptor)
        objectManager.addRequestDescriptor(saveAnswerRequestDescriptor)
        objectManager.addResponseDescriptor(saveAnswerResponseDescriptor)
        
        let barMapping = RKObjectMapping(forClass: Bar.self)
        barMapping.addAttributeMappingsFromDictionary([
            "following":"following",
            "checked_in":"checked_in",
            "liked":"liked",
            "male_followers":"male_followers",
            "female_followers":"female_followers",
            "male_check_ins":"male_check_ins",
            "female_check_ins":"female_check_ins",
            "male_likes":"male_likes",
            "female_likes":"female_likes",
            "address1":"address1",
            "address2":"address2",
            "phone_number":"phone_number",
            "website":"website",
            "name":"name",
            "bar_type":"type",
            "avatar_path":"avatar_path",
            "video_path":"video_path",
            "video_thumb_path":"video_thumb_path",
            "outside_image_path":"outside_image_path",
            "inside_image1_path":"inside_image1_path",
            "inside_image2_path":"inside_image2_path",
            "inside_image3_path":"inside_image3_path",
            "music_type":"music_type",
            "age_demographic":"age_demographic",
            "tagline":"tagline",
            "about_us":"about_us",
            "id":"id"
            ])
        var successful_status = NSMutableIndexSet(index:200)
        successful_status.addIndex(304)
        let barsResponseDescriptor = RKResponseDescriptor(mapping: barMapping, method: RKRequestMethod.Any, pathPattern: Constants.API_BARS_PATH, keyPath:"bars", statusCodes: successful_status)
        let barResponseDescriptor = RKResponseDescriptor(mapping: barMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_BAR_PATH, keyPath:"bar", statusCodes: successful_status)
        let myPlacesResponseDescriptor = RKResponseDescriptor(mapping: barMapping, method: RKRequestMethod.Any, pathPattern: Constants.API_MY_PLACES_PATH, keyPath:"bars", statusCodes: successful_status)
        let barRequestDescriptor = RKRequestDescriptor(mapping: barMapping.inverseMapping(), objectClass: Bar.self, rootKeyPath:"bars", method: RKRequestMethod.Any)
        
        objectManager.addResponseDescriptor(barsResponseDescriptor)
        objectManager.addResponseDescriptor(barResponseDescriptor)
        objectManager.addResponseDescriptor(myPlacesResponseDescriptor)
        objectManager.addRequestDescriptor(barRequestDescriptor)
        
        let promoMapping = RKObjectMapping(forClass: Promo.self)
        promoMapping.addAttributeMappingsFromDictionary([
            "id":"id",
            "name":"name",
            "category":"category",
            "bar_name":"bar_name",
            "bar_id":"bar_id",
            "description":"desc",
            "image_path":"image_path",
            "coupon_image_path":"coupon_image_path",
            "start_date_relative":"start_date_relative",
            "end_date_relative":"end_date_relative"
            ])
        
        let promosResponseDescriptor = RKResponseDescriptor(mapping: promoMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_PROMOS_PATH, keyPath: "promos", statusCodes: successful_status)
        let promoResponseDescriptor = RKResponseDescriptor(mapping: promoMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_PROMO_PATH, keyPath:"promo", statusCodes: successful_status)
        let barPromosResponseDescriptor = RKResponseDescriptor(mapping: promoMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_BAR_PROMOS_PATH, keyPath: "promos", statusCodes: successful_status)
        
        objectManager.addResponseDescriptor(promosResponseDescriptor)
        objectManager.addResponseDescriptor(promoResponseDescriptor)
        objectManager.addResponseDescriptor(barPromosResponseDescriptor)
        
        let userMapping = RKObjectMapping(forClass: User.self)
        userMapping.addAttributeMappingsFromDictionary([
            "email":"email",
            "username":"username",
            "auth_token":"auth_token",
            "dob":"dob",
            "gender":"gender",
            "num_answers":"num_answers",
            "id":"id"
            ])
        let userRequestMapping = RKObjectMapping(forClass: User.self)
        userRequestMapping.addAttributeMappingsFromDictionary([
            "email":"email",
            "username":"username",
            "dob":"dob",
            "password":"password",
            "password_confirmation":"password_confirmation",
            "gender":"gender"
            ])
        
        let registrationMapping = RKObjectMapping(forClass: User.self)
        registrationMapping.addAttributeMappingsFromDictionary([
            "email":"email",
            "username":"username",
            "password":"password",
            "password_confirmation":"password_confirmation",
            "dob":"dob",
            "gender":"gender",
            "auth_token":"auth_token",
            "num_answers":"num_answers"
            ])
        
        let loginResponseDescriptor = RKResponseDescriptor(mapping: userMapping, method: RKRequestMethod.POST, pathPattern: Constants.API_LOGIN_PATH, keyPath:"user", statusCodes: successful_status)
        let twitterLoginResponseDescriptor = RKResponseDescriptor(mapping: userMapping, method: RKRequestMethod.POST, pathPattern: Constants.API_TWITTER_LOGIN_PATH, keyPath:"user", statusCodes: NSIndexSet(index:200))
        let registerResponseDescriptor = RKResponseDescriptor(mapping: userMapping, method: RKRequestMethod.POST, pathPattern: Constants.API_REGISTER_PATH, keyPath:"user", statusCodes: NSIndexSet(index:201))
        let usersResponseDescriptor = RKResponseDescriptor(mapping: userMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_USER_PATH, keyPath:"user", statusCodes: successful_status)
        let updateUserResponseDescriptor = RKResponseDescriptor(mapping: userMapping, method: RKRequestMethod.PUT, pathPattern: Constants.API_USER_PATH, keyPath:"user", statusCodes: successful_status)
        
        let userRequestDescriptor = RKRequestDescriptor(mapping: userRequestMapping.inverseMapping(), objectClass: User.self, rootKeyPath:"user", method: RKRequestMethod.PUT)
        
        let registrationRequestDescriptor = RKRequestDescriptor(mapping: registrationMapping.inverseMapping(), objectClass: User.self, rootKeyPath:"user", method: RKRequestMethod.POST)
        
        
        objectManager.addResponseDescriptor(loginResponseDescriptor)
        objectManager.addResponseDescriptor(twitterLoginResponseDescriptor)
        objectManager.addResponseDescriptor(registerResponseDescriptor)
        objectManager.addResponseDescriptor(usersResponseDescriptor)
        objectManager.addResponseDescriptor(updateUserResponseDescriptor)
        objectManager.addRequestDescriptor(userRequestDescriptor)
        objectManager.addRequestDescriptor(registrationRequestDescriptor)
        
        let searchResultsMapping = RKObjectMapping(forClass: SearchResult.self)
        searchResultsMapping.addAttributeMappingsFromDictionary([
            "type":"type",
            "id":"id",
            "image_path":"image_path",
            "title":"title",
            "info":"info",
            "bar_data":"bar_data",
            "promo_data":"promo_data"
            ])
        let searchResponseDescriptor = RKResponseDescriptor(mapping: searchResultsMapping, method: RKRequestMethod.GET, pathPattern: Constants.API_SEARCH_PATH, keyPath:"search_results", statusCodes: successful_status)
        
        objectManager.addResponseDescriptor(searchResponseDescriptor)
        
        RKObjectManager.setSharedManager(objectManager)
    }
    
    class func setHeaders() {
        RKObjectManager.sharedManager().HTTPClient.setDefaultHeader("Accept", value: "application/vnd.check20.v1")
        var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if (defaults.objectForKey(Constants.USER_KEY) != nil) {
            var user_data = defaults.objectForKey(Constants.USER_KEY) as NSData
            if user_data.length > 0 {
                var user = NSKeyedUnarchiver.unarchiveObjectWithData(user_data) as User
                let auth_token: String? = user.auth_token
                if auth_token != nil {
                    RKObjectManager.sharedManager().HTTPClient.setDefaultHeader("Authorization", value: auth_token!)
                }
            }
        }
    }
}
