//
//  VideoPlayerController.swift
//  check20_app
//
//  Created by Chris Wood on 3/7/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

import MediaPlayer

class VideoPlayerController : MPMoviePlayerViewController {
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return UIInterfaceOrientationMask.LandscapeLeft.rawValue.hashValue
    }
    
    
}
