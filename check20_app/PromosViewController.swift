//
//  PromosViewController.swift
//  check20_app
//
//  Created by Chris Wood on 2/7/15.
//  Copyright (c) 2015 check20. All rights reserved.
//

class PromosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet var navButton: UIButton!
    @IBOutlet var headerRightButton: UIButton!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var alcohol_results = [Promo]()
    var event_results = [Promo]()
    var food_results = [Promo]()
    
    var imageCache = [String : UIImage]()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadActivityIndicator()
        actInd.startAnimating()
        
        loadPromos()
        
        setStyles()
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if !searchView.hidden {
            searchField.resignFirstResponder()
            toggleSearchView(false)
        }
    }
    
    func toggleSearchView(show: Bool) {
        UIView.transitionWithView(searchView, duration: 0.35, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.searchView.hidden = !show
            if self.searchView.hidden {
                self.searchField.resignFirstResponder()
                self.searchField.text = ""
            }
            }, completion:nil )
    }
    
    func setStyles() {
        navButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        headerRightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func loadActivityIndicator() {
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }
    
    func loadPromos() {
        let objectManager = RKObjectManager.sharedManager()
        objectManager.getObjectsAtPath(Constants.API_PROMOS_PATH, parameters: nil,
            success: { operation, mappingResult in
                
                var promos = mappingResult.array() as [Promo]
                
                if promos.count > 0 {
                    self.alcohol_results = promos.filter { $0.category == "Alcohol" }
                    self.event_results = promos.filter { $0.category == "Event" }
                    self.food_results = promos.filter { $0.category == "Food" }
                    
                    self.tableView.reloadData()
                    self.actInd.stopAnimating()
                } else {
                    var noPromosLabel = UILabel()
                    noPromosLabel.font = UIFont(name:"Roboto-Regular", size:18)
                    noPromosLabel.numberOfLines = 0
                    noPromosLabel.text = "There are currently no promos at any of the places you are following."
                    noPromosLabel.textAlignment = .Center
                    noPromosLabel.frame = CGRectMake(0, 0, self.view.frame.size.width - 10, 48)
                    noPromosLabel.sizeToFit()
                    noPromosLabel.center = self.view.center
                    noPromosLabel.textColor = UIColor.whiteColor()
                    self.view.addSubview(noPromosLabel)
                    self.view.bringSubviewToFront(noPromosLabel)
                    self.actInd.stopAnimating()
                }
                
            }, failure: { operation, error in
                println("an error occured: \(error)")
            }
        )
    }
    
    //table view delegate methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if alcohol_results.count == 0 { return 0 }
        case 1:
            if event_results.count == 0 { return 0 }
        case 2:
            if food_results.count == 0 { return 0 }
        default:
            break
        }
        return 40
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as TableViewHeader
        
        switch (section) {
        case 0:
            if alcohol_results.count == 0 { return nil }
            headerCell.titleLabel.text = "Alcohol";
        case 1:
            if event_results.count == 0 { return nil }
            headerCell.titleLabel.text = "Events";
        case 2:
            if food_results.count == 0 { return nil }
            headerCell.titleLabel.text = "Food";
        default:
            headerCell.titleLabel.text = "Other";
        }
        headerCell.setStyles()
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return alcohol_results.count
        case 1:
            return event_results.count
        case 2:
            return food_results.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("PromoCellLarge") as PromoTableViewCellLarge
        
        var promo: Promo!
        switch indexPath.section {
        case 0:
            promo = alcohol_results[indexPath.row]
        case 1:
            promo = event_results[indexPath.row]
        case 2:
            promo = food_results[indexPath.row]
        default:
            break
        }
    
        var image: UIImage? = imageCache[promo.image_path]
        
        if image == nil {
            let promo_image_url_string = Constants.ENV == "development" ? Constants.API_HOST + promo.image_path! : promo.image_path!
            let url: NSURL = NSURL(string:promo_image_url_string)!
            let request: NSURLRequest = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                if error == nil {
                    self.imageCache[promo.image_path] = UIImage(data: data)!
                    dispatch_async(dispatch_get_main_queue(), {
                        cell.promo_imageView.image = self.imageCache[promo.image_path]
                    })
                }
            })
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                cell.promo_imageView.image = image!
            })
        }
        var start_date = promo.start_date_relative
        var end_date = promo.end_date_relative
        
        cell.bar_name.text = promo.bar_name
        cell.promo_name.text = promo.name
        cell.promo_start_date.text = promo.start_date_relative
        cell.promo_end_date.text = promo.end_date_relative
        
        cell.setStyles()

        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as PromoTableViewCellLarge).setBackgroundImage()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath) as PromoTableViewCellLarge
        
        UIView.transitionWithView(cell, duration: 0.001, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            cell.bar_name.textColor = Constants.CHECK20_LIGHT_ORANGE
            cell.promo_name.textColor = Constants.CHECK20_LIGHT_ORANGE
            cell.promo_start_date.textColor = Constants.CHECK20_LIGHT_ORANGE
            cell.promo_end_date.textColor = Constants.CHECK20_LIGHT_ORANGE
            }, completion: { _ in
                UIView.transitionWithView(cell, duration: 0.55, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    cell.bar_name.textColor = UIColor.whiteColor()
                    cell.promo_name.textColor = UIColor.whiteColor()
                    cell.promo_start_date.textColor = UIColor.whiteColor()
                    cell.promo_end_date.textColor = UIColor.whiteColor()
            }, completion: nil)
        })
        var promo: Promo!
        switch indexPath.section {
        case 0:
            promo = alcohol_results[indexPath.row]
        case 1:
            promo = event_results[indexPath.row]
        case 2:
            promo = food_results[indexPath.row]
        default:
            break
        }
        self.navigationController?.pushViewController(Navigation.promo(promo, image: imageCache[promo.image_path!]), animated: true)
    }
    
    @IBAction func searchPressed(sender: UIButton) {
        toggleSearchView(searchView.hidden)
        searchField.becomeFirstResponder()
    }
    
    @IBAction func search(sender: UIButton) {
        let searchText = searchField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if countElements(searchText) > 0 {
            self.navigationController?.pushViewController(Navigation.searchResults(searchText), animated:true)
        } else {
            
        }
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func cancelSearch(sender: UIButton) {
        toggleSearchView(false)
        searchField.resignFirstResponder()
    }
    
    @IBAction func navPressed(sender: UIButton) {
        var sideSwipe: MMDrawerController = self.navigationController?.topViewController as MMDrawerController
        if sideSwipe.openSide == MMDrawerSide.Left {
            sideSwipe.closeDrawerAnimated(true, completion: nil)
        } else {
            sideSwipe.openDrawerSide(.Left, animated: true, completion: nil)
        }
    }
    
}